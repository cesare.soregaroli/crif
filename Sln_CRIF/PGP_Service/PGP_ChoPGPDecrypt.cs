﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;
using Org.BouncyCastle.X509;

namespace PGP_Service.PublicKey
{
    /// <summary>
    /// Enum Per i tipi di File
    /// </summary>
    public enum ChoPGPFileType
    {
        /// <summary>
        /// 
        /// </summary>
        Binary,
        /// <summary>
        /// 
        /// </summary>
        Text,
        /// <summary>
        /// 
        /// </summary>
        UTF8
    }
    /// <summary>
    /// Tipologia di algoritmo e relativa compressione
    /// </summary>
    public enum ChoCompressionAlgorithm
    {
        /// <summary>
        /// 
        /// </summary>
        Uncompressed = 0,
        /// <summary>
        /// 
        /// </summary>
        Zip = 1,
        /// <summary>
        /// 
        /// </summary>
        ZLib = 2,
        /// <summary>
        /// 
        /// </summary>
        BZip2 = 3
    }
    /// <summary>
    /// Chiave simmetrica e relativo algoritmo
    /// </summary>
    public enum ChoSymmetricKeyAlgorithm
    {
        /// <summary>
        /// 
        /// </summary>
        Null = 0,
        /// <summary>
        /// 
        /// </summary>
        Idea = 1,
        /// <summary>
        /// 
        /// </summary>
        TripleDes = 2,
        /// <summary>
        /// 
        /// </summary>
        Cast5 = 3,
        /// <summary>
        /// 
        /// </summary>
        Blowfish = 4,
        /// <summary>
        /// 
        /// </summary>
        Safer = 5,
        /// <summary>
        /// 
        /// </summary>
        Des = 6,
        /// <summary>
        /// 
        /// </summary>
        Aes128 = 7,
        /// <summary>
        /// 
        /// </summary>
        Aes192 = 8,
        /// <summary>
        /// 
        /// </summary>
        Aes256 = 9,
        /// <summary>
        /// 
        /// </summary>
        Twofish = 10,
        /// <summary>
        /// 
        /// </summary>
        Camellia128 = 11,
        /// <summary>
        /// 
        /// </summary>
        Camellia192 = 12,
        /// <summary>
        /// 
        /// </summary>
        Camellia256 = 13
    }

    /// <summary>
    /// Chiave pubblica algoritmo
    /// </summary>
    public enum ChoPublicKeyAlgorithm
    {
        /// <summary>
        /// 
        /// </summary>
        RsaGeneral = 1,
        /// <summary>
        /// 
        /// </summary>
        RsaEncrypt = 2,
        /// <summary>
        /// 
        /// </summary>
        RsaSign = 3,
        /// <summary>
        /// 
        /// </summary>
        ElGamalEncrypt = 16,
        /// <summary>
        /// 
        /// </summary>
        Dsa = 17,
        /// <summary>
        /// 
        /// </summary>
        EC = 18,
        /// <summary>
        /// 
        /// </summary>
        ECDH = 18,
        /// <summary>
        /// 
        /// </summary>
        ECDsa = 19,
        /// <summary>
        /// 
        /// </summary>
        ElGamalGeneral = 20,
        /// <summary>
        /// 
        /// </summary>
        DiffieHellman = 21,
        /// <summary>
        /// 
        /// </summary>
        Experimental_1 = 100,
        /// <summary>
        /// 
        /// </summary>
        Experimental_2 = 101,
        /// <summary>
        /// 
        /// </summary>
        Experimental_3 = 102,
        /// <summary>
        /// 
        /// </summary>
        Experimental_4 = 103,
        /// <summary>
        /// 
        /// </summary>
        Experimental_5 = 104,
        /// <summary>
        /// 
        /// </summary>
        Experimental_6 = 105,
        /// <summary>
        /// 
        /// </summary>
        Experimental_7 = 106,
        /// <summary>
        /// 
        /// </summary>
        Experimental_8 = 107,
        /// <summary>
        /// 
        /// </summary>
        Experimental_9 = 108,
        /// <summary>
        /// 
        /// </summary>
        Experimental_10 = 109,
        /// <summary>
        /// 
        /// </summary>
        Experimental_11 = 110
    }

    /// <summary>
    /// classe per effettuare il Decrypt del file
    /// </summary>
    public class PGP_ChoPGPDecrypt
    {
        /// <summary>
        /// Istanza di tipo PGP_ChoPGPDecrypt
        /// </summary>
        public static readonly PGP_ChoPGPDecrypt Instance = new PGP_ChoPGPDecrypt();

        private const int BufferSize = 0x10000;

        /// <summary>
        /// Algoritmo di compressione
        /// </summary>
        public ChoCompressionAlgorithm CompressionAlgorithm
        {
            get;
            set;
        }
        /// <summary>
        /// Chiave simmetrica Algoritmo
        /// </summary>
        public ChoSymmetricKeyAlgorithm SymmetricKeyAlgorithm
        {
            get;
            set;
        }
        /// <summary>
        /// Tipo di Firma
        /// </summary>
        public int PgpSignatureType
        {
            get;
            set;
        }
        /// <summary>
        /// Chiave pubblica algoritmo
        /// </summary>
        public ChoPublicKeyAlgorithm PublicKeyAlgorithm
        {
            get;
            set;
        }
        /// <summary>
        /// tipo di file
        /// </summary>
        public ChoPGPFileType FileType
        {
            get;
            set;
        }

        #region "PGP_ChoPGPDecrypt"        
        /// <summary>
        /// Costruttore
        /// </summary>
        public PGP_ChoPGPDecrypt()
        {
            CompressionAlgorithm = ChoCompressionAlgorithm.Uncompressed;
            SymmetricKeyAlgorithm = ChoSymmetricKeyAlgorithm.TripleDes;
            PgpSignatureType = PgpSignature.DefaultCertification;
            PublicKeyAlgorithm = ChoPublicKeyAlgorithm.RsaGeneral;
            FileType = ChoPGPFileType.Binary;
        }
        #endregion

        #region "Decrypt File"
        /// <summary>
        /// Funzione che Effettua il Decrypt del relativo file.
        /// </summary>
        /// <param name="inputFilePath">File di Input su cui effettuare il Decrypt</param>
        /// <param name="outputFilePath">File di Output dove salvare l'estrazione</param>
        /// <param name="privateKeyFilePath">File contenente la chiave privata</param>
        /// <param name="passPhrase">Password da applicare</param>
        public void DecryptFile(string inputFilePath, string outputFilePath, string privateKeyFilePath, string passPhrase)
        {
            if (String.IsNullOrEmpty(inputFilePath))
                throw new ArgumentException(nameof(inputFilePath));
            if (String.IsNullOrEmpty(outputFilePath))
                throw new ArgumentException(nameof(outputFilePath));
            if (String.IsNullOrEmpty(privateKeyFilePath))
                throw new ArgumentException(nameof(privateKeyFilePath));
            if (passPhrase == null)
                passPhrase = String.Empty;

            if (!File.Exists(inputFilePath))
                throw new FileNotFoundException(String.Format("Encrypted File [{0}] not found.", inputFilePath));
            if (!File.Exists(privateKeyFilePath))
                throw new FileNotFoundException(String.Format("Private Key File [{0}] not found.", privateKeyFilePath));

            using (Stream inputStream = File.OpenRead(inputFilePath))
            {
                using (Stream keyStream = File.OpenRead(privateKeyFilePath))
                {
                    using (Stream outStream = File.Create(outputFilePath))
                        Decrypt(inputStream, outStream, keyStream, passPhrase);
                }
            }
        }
        #endregion

        #region "Decryp file only Certificate"        
        /// <summary>
        /// Funzione che Effettua il Decrypt del relativo file.
        /// </summary>
        /// <param name="inputStream">File di Input su cui effettuare il Decrypt</param>
        /// <param name="outputStream">File di Output dove salvare l'estrazione</param>
        /// <param name="privateKeyStream">File contenente la chiave privata</param>
        /// <param name="passPhrase">Password da applicare</param>
        private void Decrypt(Stream inputStream, Stream outputStream, Stream privateKeyStream, string passPhrase)
        {
            if (inputStream == null)
                throw new ArgumentException(nameof(inputStream));
            if (outputStream == null)
                throw new ArgumentException(nameof(outputStream));
            if (privateKeyStream == null)
                throw new ArgumentException(nameof(privateKeyStream));
            if (passPhrase == null)
                passPhrase = String.Empty;

            PgpObjectFactory objFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(inputStream));
            // find secret key
            PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(privateKeyStream));

            PgpObject obj = null;
            if (objFactory != null)
                obj = objFactory.NextPgpObject();

            // the first object might be a PGP marker packet.
            PgpEncryptedDataList enc = null;
            if (obj is PgpEncryptedDataList)
                enc = (PgpEncryptedDataList)obj;
            else
                enc = (PgpEncryptedDataList)objFactory.NextPgpObject();


            // decrypt
            PgpPrivateKey privateKey = null;
            PgpPublicKeyEncryptedData pbe = null;
            foreach (PgpPublicKeyEncryptedData pked in enc.GetEncryptedDataObjects())
            {
                privateKey = FindSecretKey(pgpSec, pked.KeyId, passPhrase.ToCharArray());

                if (privateKey != null)
                {
                    pbe = pked;
                    break;
                }
            }

            if (privateKey == null)
                throw new ArgumentException("Secret key for message not found.");

            PgpObjectFactory plainFact = null;

            using (Stream clear = pbe.GetDataStream(privateKey))
            {
                plainFact = new PgpObjectFactory(clear);
            }

            PgpObject message = plainFact.NextPgpObject();
            if (message is PgpOnePassSignatureList)
                message = plainFact.NextPgpObject();

            if (message is PgpCompressedData)
            {
                PgpCompressedData cData = (PgpCompressedData)message;
                PgpObjectFactory of = null;

                using (Stream compDataIn = cData.GetDataStream())
                {
                    of = new PgpObjectFactory(compDataIn);
                }

                message = of.NextPgpObject();
                if (message is PgpOnePassSignatureList)
                {

                    message = of.NextPgpObject();
                    PgpLiteralData Ld = null;
                    Ld = (PgpLiteralData)message;
                    Stream unc = Ld.GetInputStream();
                    Streams.PipeAll(unc, outputStream);
                }
                else
                {
                    PgpLiteralData Ld = null;
                    Ld = (PgpLiteralData)message;
                    Stream unc = Ld.GetInputStream();
                    Streams.PipeAll(unc, outputStream);
                }
            }
            else if (message is PgpLiteralData)
            {
                PgpLiteralData ld = (PgpLiteralData)message;
                string outFileName = ld.FileName;

                Stream unc = ld.GetInputStream();
                Streams.PipeAll(unc, outputStream);
            }
            else if (message is PgpOnePassSignatureList)
                throw new PgpException("Encrypted message contains a signed message - not literal data.");
            else
                throw new PgpException("Message is not a simple encrypted file.");
        }
        #endregion

        #region "Estrae Secret Key"        
        private PgpPrivateKey FindSecretKey(PgpSecretKeyRingBundle pgpSec, long keyId, char[] pass)
        {
            PgpSecretKey pgpSecKey = pgpSec.GetSecretKey(keyId);

            if (pgpSecKey == null)
                return null;

            return pgpSecKey.ExtractPrivateKey(pass);
        }
        #endregion

        #region "DecryptFileWithSignature"
        /// <summary>
        /// Funzione che permette di Decryptare il file e verificare la relativa firma
        /// </summary>
        /// <param name="inputFilePath">Input file Path</param>
        /// <param name="outputFilePath">Output file in Uscita</param>
        /// <param name="privateKeyFilePath">Chiave Privata Path Lolcale</param>
        /// <param name="passPhrase"></param>
        /// <param name="KeyInputSignature"></param>
        public void DecryptFileWithSignature(string inputFilePath, string outputFilePath, string privateKeyFilePath, String KeyInputSignature, string passPhrase)
        {
            if (String.IsNullOrEmpty(inputFilePath)) throw new ArgumentException(nameof(inputFilePath));
            if (String.IsNullOrEmpty(outputFilePath)) throw new ArgumentException(nameof(outputFilePath));
            if (String.IsNullOrEmpty(privateKeyFilePath)) throw new ArgumentException(nameof(privateKeyFilePath));
            if (passPhrase == null) passPhrase = String.Empty;

            if (!File.Exists(inputFilePath)) throw new FileNotFoundException(String.Format("Encrypted File [{0}] not found.", inputFilePath));
            if (!File.Exists(privateKeyFilePath)) throw new FileNotFoundException(String.Format("Private Key File [{0}] not found.", privateKeyFilePath));
            if (!File.Exists(KeyInputSignature)) throw new FileNotFoundException(String.Format("Private Key Input Signature File [{0}] not found.", KeyInputSignature));

            using (Stream inputStream = File.OpenRead(inputFilePath))
            {
                using (Stream keyStream = File.OpenRead(privateKeyFilePath))
                {
                    using (Stream outStream = File.Create(outputFilePath))
                    {
                        using (Stream outSignature = File.OpenRead(KeyInputSignature)) DecryptAndVerifySignature(inputStream, outStream, keyStream, outSignature, passPhrase);
                    }
                }
            }
        }
        #endregion

        #region "DecryptAndVerifySignature"
        /// <summary>
        /// Funzione che permette di decryptare e verificare la forma dell'archivio
        /// </summary>
        /// <param name="inputStream">Input file di Ingresso</param>
        /// <param name="outputStream">Output file in Uscita</param>
        /// <param name="privateKeyStream">Chiave privata - Firma</param>
        /// <param name="inputSignature">Chiave di Input - Firma</param>
        /// <param name="passPhrase">Password</param>
        private void DecryptAndVerifySignature(Stream inputStream, Stream outputStream, Stream privateKeyStream, Stream inputSignature, string passPhrase)
        {
            if (inputStream == null)
                throw new ArgumentException(nameof(inputStream));
            if (outputStream == null)
                throw new ArgumentException(nameof(outputStream));
            if (privateKeyStream == null)
                throw new ArgumentException(nameof(privateKeyStream));
            if (passPhrase == null)
                passPhrase = String.Empty;
            if (inputSignature == null)
                throw new ArgumentException(nameof(inputSignature));

            PgpObjectFactory objFactory = new PgpObjectFactory(PgpUtilities.GetDecoderStream(inputStream));
            // find secret key
            PgpSecretKeyRingBundle pgpSec = new PgpSecretKeyRingBundle(PgpUtilities.GetDecoderStream(privateKeyStream));
            //find signature            
            //if (sList == null)
            //{
            //    throw new InvalidOperationException("PgpObjectFactory could not create signature list");
            //}
            //PgpSignature firstSig = sList[0];

            //PgpObjectFactory objFactoryKeySignature = new PgpObjectFactory(PgpUtilities.GetDecoderStream(inputSignature));            
            //PgpSignature PubSig = sListKeySignature[0];
            //if (firstSig != PubSig) { throw new InvalidOperationException("Signature is invalid"); }

            PgpObject obj = null;
            if (objFactory != null)
                obj = objFactory.NextPgpObject();


            // the first object might be a PGP marker packet.
            PgpEncryptedDataList enc = null;
            if (obj is PgpEncryptedDataList)
                enc = (PgpEncryptedDataList)obj;
            else
                enc = (PgpEncryptedDataList)objFactory.NextPgpObject();

            // decrypt
            PgpPrivateKey privateKey = null;
            PgpPublicKeyEncryptedData pbe = null;
            foreach (PgpPublicKeyEncryptedData pked in enc.GetEncryptedDataObjects())
            {
                privateKey = FindSecretKey(pgpSec, pked.KeyId, passPhrase.ToCharArray());

                if (privateKey != null)
                {
                    pbe = pked;
                    break;
                }
            }

            if (privateKey == null)
                throw new ArgumentException("Secret key for message not found.");

            PgpObjectFactory plainFact = null;

            using (Stream clear = pbe.GetDataStream(privateKey))
            {
                plainFact = new PgpObjectFactory(clear);
            }

            PgpObject message = plainFact.NextPgpObject();
            if (message is PgpOnePassSignatureList)
                message = plainFact.NextPgpObject();

            if (message is PgpCompressedData)
            {
                PgpCompressedData cData = (PgpCompressedData)message;
                PgpObjectFactory of = null;

                using (Stream compDataIn = cData.GetDataStream())
                {
                    of = new PgpObjectFactory(compDataIn);
                }

                message = of.NextPgpObject();
                if (message is PgpOnePassSignatureList)
                {

                    PgpOnePassSignatureList p1 = (PgpOnePassSignatureList)message;
                    PgpOnePassSignature ops = p1[0];

                    PgpPublicKeyRingBundle pgpRing = new PgpPublicKeyRingBundle(PgpUtilities.GetDecoderStream(inputSignature));

                    PgpPublicKey key = pgpRing.GetPublicKey(ops.KeyId);

                    if (key == null) { throw new Exception("Signature not found"); }

                    ops.InitVerify(key);

                    //if (ops.Verify(key)) { Console.Out.WriteLine("Firma Verificata"); } else { Console.Out.WriteLine("Firma NON Verificata"); };

                    PgpSignatureList p3 = (PgpSignatureList)of.NextPgpObject();
                    PgpSignature firstSig = p3[0];

                    if (ops.Verify(firstSig))
                    {
                        Console.Out.WriteLine("signature verified.");
                    }
                    else
                    {
                        Console.Out.WriteLine("signature verification failed.");
                    }

                    message = of.NextPgpObject();
                    PgpLiteralData Ld = null;
                    Ld = (PgpLiteralData)message;
                    Stream unc = Ld.GetInputStream();
                    Streams.PipeAll(unc, outputStream);
                }
                else
                {
                    PgpLiteralData Ld = null;
                    Ld = (PgpLiteralData)message;
                    Stream unc = Ld.GetInputStream();
                    Streams.PipeAll(unc, outputStream);
                }
            }
            else if (message is PgpLiteralData)
            {
                PgpLiteralData ld = (PgpLiteralData)message;
                string outFileName = ld.FileName;

                Stream unc = ld.GetInputStream();
                Streams.PipeAll(unc, outputStream);
            }
            else if (message is PgpOnePassSignatureList)
                throw new PgpException("Encrypted message contains a signed message - not literal data.");
            else
                throw new PgpException("Message is not a simple encrypted file.");
        }
        #endregion
    }
}

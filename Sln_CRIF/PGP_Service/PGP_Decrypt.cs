﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using PGP_Service.PublicKey;
using PGP_Service.PrivateKey;
using PGP_Service.Expiry;


namespace PGP_Service.PrivateKey
{
    /// <summary>
    /// Classe Utilizzata per Decriptare un file in PGP
    /// </summary>
    public class PGP_Decrypt
    {
        /// <summary>
        /// Password
        /// </summary>
        readonly System.String Password;

        /// <summary>
        /// Chiave pgp
        /// </summary>
        readonly System.String PGP_Private;

        /// <summary>
        /// Costruttore della classe
        /// </summary>
        /// <param name="_PGP_Private">Path della chiave privata</param>
        /// <param name="_password">Password</param>
        public PGP_Decrypt(System.String _PGP_Private, System.String _password)
        {
            Password = _password;
            PGP_Private = _PGP_Private;


        }

        /// <summary>
        /// Funzione per decriptare il source con chiave settata
        /// </summary>
        /// <param name="_Input">Path di Input - Crypt Document</param>
        /// <param name="_PathOutput">Path di Output</param>      
        public void decrypt(FileInfo _Input, string _PathOutput)
        {            
            try
            {
                var _pgpChoDecrypt = new PGP_ChoPGPDecrypt();                
                _pgpChoDecrypt.DecryptFile(_Input.FullName, _PathOutput, PGP_Private, Password);
            }
            catch (Exception e)
            {                
                throw new Exception(e.Message);
            }
        }

        

    }
}

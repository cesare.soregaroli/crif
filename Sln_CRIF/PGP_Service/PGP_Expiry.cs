﻿using Org.BouncyCastle.Bcpg.OpenPgp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PGP_Service.Expiry
{
    /// <summary>
    /// Pgp Expiry per verificare la validità del certificato
    /// </summary>
    public class PGP_Expiry
    {
        String _path;
        /// <summary>
        /// Costruttore della classe
        /// </summary>
        public PGP_Expiry(System.String pathKey) {
            try
            {
                _path = pathKey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Funzione per la verifica della scadenza del Certificato
        /// </summary>        
        public PgpPublicKeyItem Verify_Expiry() {
            PgpPublicKeyItem _PgpPublicKeyItem = null;
            PgpPublicKey _PgpPublicKey = null;
            try
            {
                _PgpPublicKey = ReadPublicKey();

                if (_PgpPublicKey != null)
                {

                    _PgpPublicKeyItem = new PgpPublicKeyItem
                    {
                        _DateInitialization = _PgpPublicKey.CreationTime.Date,
                        _DateExpiry = _PgpPublicKey.CreationTime.AddSeconds(_PgpPublicKey.GetValidSeconds())
                    };
                }
            }
            
            catch (Exception ex)
            {
                throw ex;
            }
            return _PgpPublicKeyItem;
        }

        private PgpPublicKey ReadPublicKey()
        {
            using (Stream keyIn = File.OpenRead(_path))
            using (var inputStream = PgpUtilities.GetDecoderStream(keyIn))
            {
                var publicKeyRingBundle = new PgpPublicKeyRingBundle(inputStream);
                var foundKey = GetFirstPublicKey(publicKeyRingBundle);
                if (foundKey != null)
                    return foundKey;
            }
            throw new ArgumentException("No encryption key found in public key ring.");
        }

        private PgpPublicKey GetFirstPublicKey(PgpPublicKeyRingBundle publicKeyRingBundle)
        {
            foreach (PgpPublicKeyRing kRing in publicKeyRingBundle.GetKeyRings())
            {
                var key = kRing.GetPublicKeys()
                    .Cast<PgpPublicKey>()
                    .Where(k => k.IsEncryptionKey)
                    .FirstOrDefault();

                if (key != null)
                    return key;
            }
            return null;
        }

        /// <summary>
        /// Oggetto contenente la chiave pubblica estratta
        /// </summary>
        public class PgpPublicKeyItem
        {
            /// <summary>
            /// Data di Creazione del certificato
            /// </summary>
           public DateTime _DateInitialization { get; set; }
            /// <summary>
            /// Data di Scadenza del certificato
            /// </summary>
            public DateTime _DateExpiry { get; set; }
        }

    }
}

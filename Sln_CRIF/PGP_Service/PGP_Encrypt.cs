﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities.IO;

namespace PGP_Service.PublicKey
{
    /// <summary>
    /// Classe Utilizzata per Criptare un file in PGP
    /// </summary>    
    public class PGP_Encrypt
    {

        /// <summary>
        /// Chiave pubblica
        /// </summary>
        public PgpPublicKey PublicKey { get; private set; }

        /// <summary>
        /// Metodo per effettuare il Crypt dei dati
        /// </summary>
        /// <param name="publicKeyPath">Path chiave pubblica</param>
        public PGP_Encrypt(string publicKeyPath)
        {
            if (!File.Exists(publicKeyPath))
                throw new ArgumentException("Public key file not found", "publicKeyPath");
            PublicKey = ReadPublicKey(publicKeyPath);
        }

        /// <summary>
        /// Metodo per effettuare il Crypt dei dati
        /// </summary>
        /// <param name="inputFile">Input File</param>
        /// <param name="outputFile">Output File</param>
        /// <param name="armor">Armor</param>
        /// <param name="withIntegrityCheck">Integrity Check</param>
        public void EncryptFile(string inputFile, string outputFile, bool armor, bool withIntegrityCheck)
        {
            using (MemoryStream outputBytes = new MemoryStream())
            {
                PgpCompressedDataGenerator dataCompressor = new PgpCompressedDataGenerator(CompressionAlgorithmTag.Zip);
                PgpUtilities.WriteFileToLiteralData(dataCompressor.Open(outputBytes), PgpLiteralData.Binary, new FileInfo(inputFile));

                dataCompressor.Close();
                PgpEncryptedDataGenerator dataGenerator = new PgpEncryptedDataGenerator(SymmetricKeyAlgorithmTag.Cast5, withIntegrityCheck, new SecureRandom());

                dataGenerator.AddMethod(PublicKey);
                byte[] dataBytes = outputBytes.ToArray();

                using (Stream outputStream = File.Create(outputFile))
                {
                    if (armor)
                    {
                        using (ArmoredOutputStream armoredStream = new ArmoredOutputStream(outputStream))
                        {
                            WriteStream(dataGenerator.Open(armoredStream, dataBytes.Length), ref dataBytes);
                        }
                    }
                    else
                    {
                        WriteStream(dataGenerator.Open(outputStream, dataBytes.Length), ref dataBytes);
                    }
                }
            }

        }

        /// <summary>
        /// Scrittura dello Stream
        /// </summary>
        /// <param name="inputStream">File in Stream di Input</param>
        /// <param name="dataBytes">Data in formato Byte</param>
        public static void WriteStream(Stream inputStream, ref byte[] dataBytes)
        {
            using (Stream outputStream = inputStream)
            {
                outputStream.Write(dataBytes, 0, dataBytes.Length);
            }
        }

        #region Public Key
        /// <summary>
        /// Lettura chiave pubblica
        /// </summary>
        /// <param name="publicKeyPath">Path chiave pubblica</param>
        /// <returns></returns>
        private PgpPublicKey ReadPublicKey(string publicKeyPath)
        {
            using (Stream keyIn = File.OpenRead(publicKeyPath))
            using (Stream inputStream = PgpUtilities.GetDecoderStream(keyIn))
            {
                PgpPublicKeyRingBundle publicKeyRingBundle = new PgpPublicKeyRingBundle(inputStream);
                PgpPublicKey foundKey = GetFirstPublicKey(publicKeyRingBundle);
                if (foundKey != null)
                    return foundKey;
            }
            throw new ArgumentException("No encryption key found in public key ring.");
        }
        /// <summary>
        /// Estrae la prima chiave pubblica
        /// </summary>
        /// <param name="publicKeyRingBundle"></param>
        /// <returns></returns>
        private PgpPublicKey GetFirstPublicKey(PgpPublicKeyRingBundle publicKeyRingBundle)
        {
            foreach (PgpPublicKeyRing kRing in publicKeyRingBundle.GetKeyRings())
            {
                PgpPublicKey key = kRing.GetPublicKeys()
                .Cast<PgpPublicKey>()
                .Where(k => k.IsEncryptionKey)
                .FirstOrDefault();
                if (key != null)
                    return key;
            }
            return null;
        }
        #endregion

    }
}



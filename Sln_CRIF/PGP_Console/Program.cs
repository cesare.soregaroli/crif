﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PGP_Service.PublicKey;
using PGP_Service.PrivateKey;
using System.IO;
using PGP_Service.Expiry;

namespace PGP_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //PATH dei  File
            var File_INPUT_CRIF = System.Configuration.ConfigurationManager.AppSettings["File_INPUT_CRIF"].ToString();
            Console.WriteLine(File_INPUT_CRIF);

            var File_OUTPUT_CRIF = System.Configuration.ConfigurationManager.AppSettings["File_OUTPUT_CRIF"].ToString();
            Console.WriteLine(File_OUTPUT_CRIF);

            var Path_INPUT_CRIF = System.Configuration.ConfigurationManager.AppSettings["Path_INPUT_CRIF"].ToString();
            Console.WriteLine(Path_INPUT_CRIF);

            var Path_OUTPUT_CRIF = System.Configuration.ConfigurationManager.AppSettings["Path_OUTPUT_CRIF"].ToString();
            Console.WriteLine(Path_OUTPUT_CRIF);

            var Path_OUTPUT_DECRIPTED_CRIF = System.Configuration.ConfigurationManager.AppSettings["Path_OUTPUT_DECRIPTED_CRIF"].ToString();
            Console.WriteLine(Path_OUTPUT_DECRIPTED_CRIF);

            var _pathInput = System.IO.Path.Combine(Path_INPUT_CRIF, File_INPUT_CRIF);
            Console.WriteLine(_pathInput);
            var _pathOutput = System.IO.Path.Combine(Path_OUTPUT_CRIF, File_OUTPUT_CRIF);
            Console.WriteLine(_pathOutput);

            var _pathOutputDecripted = System.IO.Path.Combine(Path_OUTPUT_DECRIPTED_CRIF, File_OUTPUT_CRIF);
            Console.WriteLine(_pathOutput);

            var CRIF_PWD = System.Configuration.ConfigurationManager.AppSettings["CRIF_PWD"].ToString();
            Console.WriteLine(CRIF_PWD);

            //Path Certificati - Pubblico
            var PGP_Public = Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["PGP_Public"].ToString());
            Console.WriteLine(PGP_Public);
            //Path Certificati - Privato
            var PGP_Private = Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["PGP_Private"].ToString());
            Console.WriteLine(PGP_Private);
            //Path contenente la chiave della signature pubblica
            var PGP_Signature_Pub = Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["PGP_Signature_Private"].ToString());
            Console.WriteLine(PGP_Signature_Pub);
            try
            {
                //PGP_Encrypt.EncryptFile(_pathInput, _pathOutput, PGP_Public, true, true);
                //var _pgpEncrypt = new PGP_Encrypt(PGP_Public);
                //_pgpEncrypt.EncryptFile(_pathInput, _pathOutput,true,true);

                //var _pgpDecrypt = new PGP_Decrypt( PGP_Private, CRIF_PWD);
                //_pgpDecrypt.decrypt(_pathOutput, _pathOutputDecripted, PGP_Private);

                //var _pgpChoDecrypt = new PGP_ChoPGPDecrypt();
                //_pgpChoDecrypt.DecryptFile("C:\\Progetti\\Locali_Dati\\CRIF\\Output\\MICRODATA_0900009848501_20191223105917480.zip.encrypted.signed.pgp",
                //                "C:\\Progetti\\Locali_Dati\\CRIF\\OutPut_Decripted\\MICRODATA_0900009848501_20191223105917480.zip",
                //                "C:\\Progetti\\Locali_Prova\\CRIF\\CRIF\\Sln_CRIF\\PGP_Console\\PGP_Certificate\\SIT\\MicroDataGroup.sec",
                //                "$Microdata2019");


                var _pgpChoDecrypt = new PGP_ChoPGPDecrypt();
                _pgpChoDecrypt.DecryptFile(_pathInput, _pathOutputDecripted, PGP_Private, "$Microdata2019");

                
               // _pgpChoDecrypt.DecryptFileWithSignature(_pathOutput, _pathOutputDecripted, PGP_Private, PGP_Signature_Pub, "$Microdata2019");

                //var _pgpVerify = new PGP_Expiry(PGP_Public);
                //PGP_Service.Expiry.PGP_Expiry.PgpPublicKeyItem _PgpPublicKeyItem = _pgpVerify.Verify_Expiry();                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

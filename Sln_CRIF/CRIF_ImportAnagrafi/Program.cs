﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.DevExpressMdg;
using CRIF_KYC.ExcelMDG;
using CRIF_KYC.Configuration;
using System.IO;
using System.Data;
using CRIF_KYC.Elaborazione;

namespace CRIF_KYC
{
    class Program
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            
            FileInfo _fileInfo = null;
            try
            {
                log.Info($"Inizio CRIF_ImportAnagrafi");
                Crif_Configuration.ParameterAttribute();
                DirectoryInfo _dirInfo = new DirectoryInfo(Configuration.Crif_Configuration.Excel_Input);
                if (_dirInfo.GetFiles().Length > 0)
                {
                    _fileInfo = _dirInfo.GetFiles().FirstOrDefault();

                    log.Info($"File da Lavorare con nome: {_fileInfo.FullName}");
                    Crif_Excel oCrif_ExcelNDG = new Crif_Excel();
                    DataTable source_KYC_NDG = oCrif_ExcelNDG.load_KYC_Scadenziario(_fileInfo.FullName, 0);
                    Crif_Elaborazione _Crif_Elaborazione = new Crif_Elaborazione();
                    _Crif_Elaborazione.Gestione_ExcelDati(source_KYC_NDG);
                    log.Info($"Copia del file eseguito.");
                    File.Copy(_fileInfo.FullName, Path.Combine(Crif_Configuration.Excel_Output, _fileInfo.Name));
                    log.Info($"Fine CRIF_ImportAnagrafi");
                }
                else { log.Info("Nessun file da Elaborare nella cartella di Input"); }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

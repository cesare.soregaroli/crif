﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.DevExpressMdg;
using System.IO;
using CRIF_KYC.Configuration;

namespace CRIF_KYC.ExcelMDG
{
    class Crif_Excel
    {

        public System.Data.DataTable load_KYC_Scadenziario(System.String _pathExcel, System.Int16 _idSheet) {
            
            System.Data.DataTable _dtKYC_Scadenziario = null;
            try
            {
                ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_pathExcel, DevExpress.Spreadsheet.DocumentFormat.Xlsx); ;
                _dtKYC_Scadenziario = oDevExpressExcel.ExportDataTable(_idSheet);

            }
            catch (Exception)
            {
                throw;
            }
            return _dtKYC_Scadenziario;
        }
    }
}

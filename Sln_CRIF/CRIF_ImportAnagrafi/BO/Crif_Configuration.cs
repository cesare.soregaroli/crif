﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CRIF_KYC.Configuration
{

    static class Crif_Configuration
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string _data = DateTime.Now.ToString("yyyyMMdd_HHmmss");


        /// <summary>
        /// Path per prendere in input il file da lavorare
        /// </summary>
        static public System.String Excel_Input { get => Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["Excel_Input_Anagrafe"].ToString()); }

        /// <summary>
        /// Path dove depositare in outpot
        /// </summary>
        static public System.String Excel_Output {
            get; set;
        }

        /// <summary>
        /// Connessione al Database
        /// </summary>
        static public System.String CONNECTION_DATABASE
        {
            get => System.Configuration.ConfigurationManager.AppSettings["CONNECTION_DATABASE"].ToString();
        }


        /// <summary>
        /// Stampo Il contenuto dei parametri settati
        /// </summary>
        static public void ParameterAttribute()
        {
            try
            {
                Excel_Output = Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["Excel_Output_Anagrafe"].ToString());
                if (Directory.Exists(Excel_Output)) {
                    Directory.CreateDirectory(Excel_Output);                    
                }
                Excel_Output = Path.Combine(Excel_Output, _data);
                Directory.CreateDirectory(Excel_Output);
                //log.Info($"Path SFTP : {Crif_Configuration.CRIF_Input_SFTP.ToString()}");
                //log.Info($"Path SFTP Backup : {Crif_Configuration.CRIF_Input_SFTP_BACKUP.ToString()}");
                //log.Info($"Path SFTP Decrypted : {Crif_Configuration.CRIF_Input_SFTP_DECRYPTED.ToString()}");                
                //log.Info($"Path Work : {Crif_Configuration.CRIF_Input_WORK.ToString()}");
                //log.Info($"Path Work Backup : {Crif_Configuration.CRIF_Input_WORK_BACKUP.ToString()}");
                //log.Info($"Path Documentary : {Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY.ToString()}");

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}

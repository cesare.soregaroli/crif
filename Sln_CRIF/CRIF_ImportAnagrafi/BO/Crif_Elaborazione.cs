﻿using System;
using System.Collections.Generic;
using Oracle.DataAccess.Client;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.Configuration;
using System.Data;

namespace CRIF_KYC.Elaborazione
{
    class Crif_Elaborazione
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public void Gestione_ExcelDati(DataTable _dt) {
            try
            {
                if (_dt != null && _dt.Rows.Count > 0)
                {
                    log.Info($"Inizio Annullo Posizioni");
                    Gestione_AnnulloSalvataggioDati();
                    log.Info($"Fine Annullo Posizioni");
                    System.Int64 _ndg;
                    System.String _rischio = string.Empty;
                    System.Int32 _indice = 0;
                    log.Info($"Dati da lavorare. Numero di record {_dt.Rows.Count}");
                    foreach (DataRow _rw in _dt.Rows) {
                        if(!string.IsNullOrEmpty(_rw[1].ToString())) { 
                        _indice += 1;
                        _ndg = Convert.ToInt64(_rw[1].ToString());
                        _rischio = _rw[3].ToString();
                        log.Info($"Numero record : {_indice}");
                        log.Info($"NDG : {_ndg}");
                        log.Info($"Rischio : {_rischio}");
                        Console.WriteLine($"Numero Record {_indice} - NDG: { _ndg} - Rischio: { _rischio}");
                        Gestione_SalvataggioDati(_ndg, _rischio);
                        }
                    }
                }
                else {
                    log.Info("Nessun dato presente nell'excel");
                
                }

            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }
        }

        private void Gestione_AnnulloSalvataggioDati()
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;

            try
            {
                cn = new OracleConnection(Crif_Configuration.CONNECTION_DATABASE);
                cmd = new OracleCommand("UPDATE PD_ANAGRAFICA_CRIF SET CRIF_DISABLED = 1 ", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                
                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {

                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE PREGRESSOPRE: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {
                    
                    var _idRet = cmd.ExecuteNonQuery();
                    
                    if (_idRet < 1)
                    {
                        throw new Exception($"Update CRIF_DISABLED Non corretta per PD_ANAGRAFICA_CRIF");
                        //Da vedere se cancellare il file
                    }
                    else
                    {
                        log.Info($"Update CRIF_DISABLED effettuato correttamente per il PD_ANAGRAFICA_CRIF");
                    }

                }
            }
            catch (Exception ex)
            {

                log.Error($"ERRORE IN FASE DI UPDATE per il CRIF_DISABLED " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }

        }

        private void Gestione_SalvataggioDati(System.Int64 _codNDG , System.String _rischio)
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;
            
            try
            {
                cn = new OracleConnection(Crif_Configuration.CONNECTION_DATABASE);
                cmd = new OracleCommand("INSERT INTO PD_ANAGRAFICA_CRIF (CRIF_RISCHIO,CODICE_ANAGRAFE,CRIF_DATA_INSERIMENTO) values (:CRIF_RISCHIO,:CODICE_ANAGRAFE, :CRIF_DATA_INSERIMENTO)", cn);
                cmd.CommandType = System.Data.CommandType.Text;
                
                cmd.Parameters.Add(new OracleParameter("CRIF_RISCHIO", OracleDbType.NVarchar2, System.Data.ParameterDirection.Input));
                cmd.Parameters["CRIF_RISCHIO"].Value = _rischio;
                cmd.Parameters.Add(new OracleParameter("CODICE_ANAGRAFE", OracleDbType.Int64, System.Data.ParameterDirection.Input));
                cmd.Parameters["CODICE_ANAGRAFE"].Value = _codNDG;
                cmd.Parameters.Add(new OracleParameter("CRIF_DATA_INSERIMENTO", OracleDbType.Date, System.Data.ParameterDirection.Input));
                cmd.Parameters["CRIF_DATA_INSERIMENTO"].Value = System.DateTime.Now;
                
                                                
                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {
                    
                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE PREGRESSOPRE: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {
                    log.Info("INIZIO Esecuzione della query");
                    var _idRet = cmd.ExecuteNonQuery();
                    log.Info("FINE Esecuzione della query");
                    if (_idRet < 1)
                    {
                        throw new Exception($"INSERT CRIF_RISCHIO Non corretta per il CODICE_ANAGRAFE {_codNDG}");
                        //Da vedere se cancellare il file
                    }
                    else
                    {
                        log.Info($"INSERT CRIF_RISCHIO effettuato correttamente per il CODICE_ANAGRAFE : {_codNDG}");
                    }

                }
            }
            catch (Exception ex)
            {
                
                log.Error($"ERRORE IN FASE DI INSERT per il CODICE_ANAGRAFE NUMERO:{_codNDG} " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }

        }
    }
}

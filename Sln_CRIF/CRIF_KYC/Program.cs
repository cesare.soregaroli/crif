﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.Zip;
using CRIF_KYC.Mail;
using CRIF_KYC.Certificate;
using CRIF_KYC.Configuration;
using CRIF_KYC.ExcelMDG;
using CRIF_KYC.Lavorazione;
using System.IO;
using CRIF_KYC.Verifica;
using CRIF_KYC.Errore;
using CRIF_KYC.Warning;
using CRIF_KYC.XMLSerialize;
using CRIF_KYC.XMLConfigurazione;

namespace CRIF_KYC
{
    class Program
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static Crif_Errori _Crif_Errori = new Crif_Errori();
        private static List<Crif_Warning> _Crif_Warning = new List<Crif_Warning>();
        //ho tenuto la vecchia gestione per comodità
        private static List<System.String> _List_Tipo_Esecuzione = new List<string> { };

        private static CRIF oCRIF = new CRIF();

        static void Main(string[] args)
        {
            log.Info("Begin - DEUTSCHE_CRIF_MULTIMEDIA");
            _Crif_Errori.Errore_List = new List<string> { };
            Crif_XMLSerialize _Crif_XMLSerialize = new Crif_XMLSerialize();
            try
            {                
                CRIF oCRIF = _Crif_XMLSerialize.SerializeXMLToCRIF_Project();                
                if (oCRIF != null)
                {
                    log.Info("Oggetto XML Configurazione Caricato Correttamente");
                    log.Info($"Configurazioni prelevate : {oCRIF.CRIF_Project.Count}");
                    foreach (CRIF_Project _oCrifProject in oCRIF.CRIF_Project)
                    {
                        if (_oCrifProject.CRIF_Tipo_Esecuzione_Enable)
                        {
                            try
                            {
                                log.Info($"Procedura {_oCrifProject.CRIF_Tipo_Esecuzione} Abilitata");
                                _List_Tipo_Esecuzione.Add(_oCrifProject.CRIF_Tipo_Esecuzione);
                                Crif_Configuration.VerifyPrincipalFolder(_oCrifProject);
                                Crif_Configuration.LogParameterAttribute();
                                Procedure_Deutsche_Crif_Multimedia();
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex, ex.Message);
                                if (ex.InnerException != null) { log.Error(ex, ex.InnerException.StackTrace); }
                                _Crif_Errori.Errore = true;
                                _Crif_Errori.Errore_List.Add(ex.Message);
                                log.Error(ex, ex.StackTrace);
                            }
                        }
                        else
                        {
                            log.Info($"Procedura {_oCrifProject.CRIF_Tipo_Esecuzione} Non Abilitata");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex, ex.Message);
                if (ex.InnerException != null) { log.Error(ex, ex.InnerException.StackTrace); }
                _Crif_Errori.Errore = true;
                _Crif_Errori.Errore_List.Add(ex.Message);
                log.Error(ex, ex.StackTrace);
            }
            finally
            {
                //effettuo una verifica per quanto riguarda l'analisi degli warning presenti per CARTE e SPORTELLI
                if (_Crif_Warning.Count > 0)
                {
                    _Crif_XMLSerialize = new Crif_XMLSerialize();
                    CRIF _oCRIF = _Crif_XMLSerialize.SerializeXMLToCRIF_Project();                    
                    SendMailWarning(_Crif_Warning, _oCRIF);
                }
                //verifico se vi sono degli errori gestiti all'interno dell'applicazione
                if (_Crif_Errori.Errore)
                {
                    Crif_Mail _Crif_Mail = new Crif_Mail();
                    System.String _log_File_Error = String.Empty;
                    _log_File_Error = _Crif_Mail.CreateLogfile(_Crif_Errori.Errore_List);
                    log.Info(_log_File_Error);
                    List<System.String> _errorFile = null;
                    if (File.Exists(_log_File_Error)) { _errorFile = new List<string> { _log_File_Error }; }
                    SendMail(_errorFile);
                    log.Info($"Cancellazione file di errore nella cartella dei Log");
                }
                log.Info("End - DEUTSCHE_CRIF_MULTIMEDIA");
                createMailLogFile();
            }
        }



        #region "SendMail"        
        /// <summary>
        /// Funzione invio Mail di errore 
        /// </summary>
        /// <param name="ex">Eccezione</param>
        private static void SendMail(List<System.String> _errorFile)
        {
            Crif_Mail _Crif_Mail = new Crif_Mail();
            Boolean _invioMailEccezione = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_eccezione"].ToString());
            try
            {
                log.Info("Start => SendMail");
                _Crif_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                _Crif_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_to"].ToString();
                _Crif_Mail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_cc"].ToString();
                _Crif_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString());
                _Crif_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();
                _Crif_Mail.email_eccezione_to = System.Configuration.ConfigurationManager.AppSettings["email_eccezione_to"].ToString();
                _Crif_Mail.email_Attachement = _errorFile;
                //invio mail a Microframe
                _Crif_Mail.SendMailError(true);
                if (_invioMailEccezione)
                {
                    //Invio Mail con stack dell'errore
                    _Crif_Mail.SendMailError(false);
                    log.Info("End => SendMail");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void createMailLogFile()
        {
            System.String _cartellaLog = System.DateTime.Now.ToString("yyyy-MM-dd");
            System.String _pathCartella = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log", _cartellaLog);
            List<System.String> _listFile = new List<string> { };
            try
            {
                log.Info($"Cartella Dei Log {_pathCartella}");
                NLog.LogManager.Shutdown();
                if (Directory.GetFiles(_pathCartella).Length > 0)
                {
                    _listFile = Directory.GetFiles(_pathCartella, "*", SearchOption.TopDirectoryOnly).ToList();
                }
                InviaMailLog(_listFile);
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static void InviaMailLog(List<System.String> _LogFile)
        {
            Crif_Mail _Crif_Mail = new Crif_Mail();            
            try
            {                
                _Crif_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["MailFrom_Log"].ToString();
                _Crif_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["MailTo_Log"].ToString();                
                _Crif_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["Abilitazione_Mail_Log"].ToString());
                _Crif_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["MailSmtp_Log"].ToString();                
                _Crif_Mail.email_Attachement = _LogFile;
                
                _Crif_Mail.SendMailLog();
               
            }
            catch (Exception)
            {
                throw;
            }
        }

        static void SendMailWarning(List<Crif_Warning> _ListWarning , CRIF _oCRIF)
        {
            Crif_Mail _Crif_Mail = new Crif_Mail();

            try
            {

                foreach (var _tipoEsecuzione in _List_Tipo_Esecuzione)
                {
                    log.Info("Start => SendMail _tipoEsecuzione");

                    var NumeroElementi = _ListWarning.Where(x => x.Warning_Tipo_Progetto.Equals(_tipoEsecuzione)).Count();
                    if (NumeroElementi > 0)
                    {
                        CRIF_Project _ItemCRIF = _oCRIF.CRIF_Project.Where(X => X.CRIF_Tipo_Esecuzione.Equals(_tipoEsecuzione)).FirstOrDefault();
                        if (_ItemCRIF != null)
                        {
                            _Crif_Mail.email_from = _ItemCRIF.CRIF_MAIL.CRIF_Project_from;
                            _Crif_Mail.email_to = _ItemCRIF.CRIF_MAIL.CRIF_Project_to;
                            _Crif_Mail.email_cc = _ItemCRIF.CRIF_MAIL.CRIF_Project_cc;
                            _Crif_Mail.email_sendemail = _ItemCRIF.CRIF_MAIL.CRIF_Project_sendemail;
                            _Crif_Mail.email_smtpserver = _ItemCRIF.CRIF_MAIL.CRIF_Project_smtpserver;
                            List<System.String> _ListaSegnalazioni = _ListWarning.Where(x => x.Warning_Tipo_Progetto.Equals(_tipoEsecuzione)).Select(a => a.Warning_Element).ToList();
                            //invio mail a Microframe
                            _Crif_Mail.SendMailWarning(_tipoEsecuzione, _ListaSegnalazioni);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "Procedure_Deutsche_Crif_Multimedia"

        /// <summary>
        /// Procedura che permette di avere a disposizione i dati elaborati da Esporre al Documentale
        /// </summary>
        private static void Procedure_Deutsche_Crif_Multimedia()
        {
            Crif_Certificate _crif_Certificate = new Crif_Certificate();
            Crif_Zip _crif_Zip = new Crif_Zip();
            Crif_Lavorazione _ocrif_Lavorazione = new Crif_Lavorazione();
            List<FileInfo> _lstZipFile = new List<FileInfo> { };
            Crif_Verifica _Crif_Verifica = new Crif_Verifica();

            Boolean _ret = true;
            try
            {
                //ripulisco la cartella di destinazione da tutte le cartelle che risultano vuote e non piu usate.
                _ocrif_Lavorazione.Delete_Folder_Empty(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY);
                //Effettuo la verifica del certificato in maniera da evitare errori in esecuzione                
                _ret = _crif_Certificate.Crif_Certificate_Verify();
                if (_ret)
                {
                    log.Info("Verifica Validità certificato => Esito Positivo");
                    _lstZipFile = _crif_Zip.ExtractList_Zip();

                    if (_lstZipFile != null && _lstZipFile.Count > 0)
                    {
                        foreach (FileInfo _fileInfo in _lstZipFile)
                        {

                            log.Info($"Effettuo la copia del file Cryptato spostantolo dall'SFTP del cliente {_fileInfo.Name}");
                            _crif_Zip.Copy_FileEnCrypted(_fileInfo);
                            log.Info($"Effettuo la Decrypt del file : {_fileInfo.Name}");
                            _crif_Certificate._Crif_Warning = _Crif_Warning;
                            _crif_Certificate.DecryptFile(_fileInfo);
                            _Crif_Warning = _crif_Certificate._Crif_Warning;
                            log.Info($"Effettuo l'estrazione dei file nella cartella di lavoro {Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, _fileInfo.Name)}");
                            //ritorno il nomer della cartella dove sono stati disinzippati i file con relativo =>KYC_Scadenziario => zip relativo per ogni riga                            
                            _Crif_Verifica.CopiaFileInWork(_fileInfo);
                            log.Info($"Cancellazione dall'SFTP del file : {_fileInfo.Name}");
                            File.Delete(_fileInfo.FullName);
                        }

                        log.Info("Effettuo Analisi sui file da Lavorare - Analizzo lo Scadenziario con i relativi archivi");
                        _Crif_Verifica._Crif_Warning = _Crif_Warning;
                        _Crif_Verifica._VerificaFileScadenziario();
                        _Crif_Warning = _Crif_Verifica._Crif_Warning;
                        //log.Info($"Cartella di lavoro Estratta con file KYC_Scadenziario + relativi ZIP {_pathExtractFile}");

                        _ocrif_Lavorazione._Crif_Warning = _Crif_Warning;
                        _ocrif_Lavorazione.workExtractZipFile();
                        _Crif_Warning = _ocrif_Lavorazione._Crif_Warning;
                    }


                    //Cancellazione delle cartelle di lavoro
                    _Crif_Verifica.DeleteWorkFolder();
                }
                else
                {
                    log.Info("Verifica Validità certificato => Esito Negativo");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}

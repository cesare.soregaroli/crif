﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CRIF_KYC.Configuration;
using CRIF_KYC.Zip;
using CRIF_KYC.Warning;

namespace CRIF_KYC.Verifica
{
    public class Crif_Verifica
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Permette di Tenere traccia degli Warning
        /// </summary>
        public List<Crif_Warning> _Crif_Warning { get; set; }

        public void _VerificaFileScadenziario()
        {
            try
            {
                List<FileInfo> _List_Zip = new List<FileInfo> { };
                List<System.IO.FileInfo> _ListScadenziario = new List<FileInfo> { };
                List<System.IO.DirectoryInfo> _ListFolder = new List<DirectoryInfo> { };
                List<System.String> _ListErrore = new List<string> { };
                if (!Directory.Exists(Crif_Configuration.CRIF_Input_WORK)) { log.Error($"Path WORK non presente : {Crif_Configuration.CRIF_Input_WORK}"); throw new IOException("Exceptio IO - Cartella non presente"); }

                DirectoryInfo _DIR_CRIF_Work = new DirectoryInfo(Crif_Configuration.CRIF_Input_WORK);
                _List_Zip = _DIR_CRIF_Work.GetFiles("*.*").Where(file => string.Equals(file.Extension, ".csv", StringComparison.InvariantCultureIgnoreCase)).ToList();
                if (_List_Zip == null || _List_Zip.Count == 0) { log.Info($"Nessun File presente nella cartella {Crif_Configuration.CRIF_Input_WORK}"); }
                System.Boolean _esito = false;
                foreach (var _fileInfo in _List_Zip)
                {
                    log.Info($"Verifico il File Scadenziario con nome : {_fileInfo.Name}");
                    _esito = _verificaScadenziario(_fileInfo.FullName, ref _ListFolder, ref _ListErrore);
                    if (!_esito)
                    {
                        log.Info($"Aggiunto Scadenziario Non valido da cancellare : {_fileInfo.Name}");
                        _ListScadenziario.Add(_fileInfo);
                    }
                    ///else { _ListFolder.Clear(); }
                }
                //effettuo la cancellazione dei File e Folder che non devono essere lavorati                
                _deleteFolderNotCorrect(_ListFolder, _ListScadenziario);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Funzione per effettuare la verifica del file scadenziario
        /// </summary>
        /// <param name="_PathInput">Path contenente lo scadenziario</param>
        /// <param name="_ListFolder">Lista delle cartelle</param>
        /// <param name="_ListErrore">Lista defli errori</param>
        /// <returns></returns>
        private System.Boolean _verificaScadenziario(System.String _PathInput, ref List<System.IO.DirectoryInfo> _ListFolder, ref List<System.String> _ListErrore)
        {
            System.Boolean _esito = true;
            try
            {
                if (Crif_Configuration.CRIF_Tipo_Esecuzione.Equals("SPORTELLI"))
                {
                    _esito = _verificaScadenziario_NDG(_PathInput, ref _ListFolder, ref _ListErrore);
                }
                else if (Crif_Configuration.CRIF_Tipo_Esecuzione.Equals("CARTE"))
                {
                    _esito = _verificaScadenziario_CODICEFISCALE(_PathInput, ref _ListFolder, ref _ListErrore);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _esito;
        }

        private System.Boolean _verificaScadenziario_NDG(System.String _PathInput, ref List<System.IO.DirectoryInfo> _ListFolder, ref List<System.String> _ListErrore)
        {
            System.Boolean _esito = true;
            try
            {
                DirectoryInfo _DIR_CRIF_Work = new DirectoryInfo(Crif_Configuration.CRIF_Input_WORK);
                FileInfo _FileInfoCSV = new FileInfo(_PathInput);
                var oDevExpressExcel = new CRIF_KYC.DevExpressMdg.ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_PathInput, DevExpress.Spreadsheet.DocumentFormat.Csv);
                var _dtDati = oDevExpressExcel.ExportDataTable(0);

                if (_dtDati is null || _dtDati.Rows.Count == 0)
                {
                    log.Info($"Nessun Dato presente nel CSV selezionato : {_FileInfoCSV.Name}");
                    ///in questo caso significa che il CSV è vuoto e non è necessario proseguire con l'elaborazione
                    //PopolaScarti(_FileInfoCSV);
                    _Crif_Warning.Add(new Crif_Warning()
                    {
                        Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                        Warning_Element = $"Nessun Dato presente nel CSV selezionato : {_FileInfoCSV.Name}"
                    });
                    _esito = false;
                }
                else
                {
                    foreach (System.Data.DataRow _Record in _dtDati.Rows)
                    {

                        //Verifico che la provenienza sia corretta
                        if (_Record["Provenienza"] == System.DBNull.Value)
                        {
                            _Crif_Warning.Add(new Crif_Warning()
                            {
                                Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente un valore di provenienza vuoto"
                            });
                            _esito = false;
                        }
                        else
                        {
                            System.String _Provenienza = _Record["Provenienza"].ToString();
                            if (!_Provenienza.ToUpper().Equals("S"))
                            {
                                _Crif_Warning.Add(new Crif_Warning()
                                {
                                    Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                    Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente un valore di provenienza non corretto : {_Provenienza}"
                                });
                                _esito = false;
                            }
                        }

                        //Estraggo il valore dell'NDG presente sulla riga
                        System.String _ndg = _Record["NDG"].ToString();
                        if (!string.IsNullOrEmpty(_ndg))
                        {
                            if (_DIR_CRIF_Work.GetDirectories($"*{_ndg}*").Length == 0)
                            {
                                _ListErrore.Add($"Cartella Non Trovata per NDG {_ndg}");
                                _Crif_Warning.Add(new Crif_Warning()
                                {
                                    Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                    Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente l'NDG {_ndg}, ma non ha associato nessun archivio"
                                });
                                _esito = false;
                            }
                            else
                            {
                                var _directory = _DIR_CRIF_Work.GetDirectories($"*{_ndg}*").First();
                                if (_directory.Exists)
                                {
                                    _ListErrore.Add($"Cartella Trovata con nome :{_directory.Name}");
                                    _ListFolder.Add(_directory);
                                }
                            }
                        }

                        //Ho commentato questo pezzo in attesa che Federici ci faccia sapere come vuole muoversi in merito a questa verifica sul biometricCheckResult

                        ////effettuo la verifica inerente al fatto che il check Biometrico sia impostato correttamente a 1 se a 0 l'archivio deve essere spostato.
                        //System.String _BiometricValue = _Record["biometricCheckResult"].ToString();
                        //if (!string.IsNullOrEmpty(_BiometricValue) && _BiometricValue.Equals("0")) {
                        //    var _directory = _DIR_CRIF_Work.GetDirectories($"*{_ndg}*").First();
                        //    if (_directory.Exists)
                        //    {
                        //        _ListErrore.Add($"Cartella Trovata con nome :{_directory.Name} per NDG {_ndg} biometricCheckResult = 0");
                        //        _ListFolder.Add(_directory);
                        //    }
                        //    //non metto esito false perchè va comunque lavorate le altre cartelle presenti.
                        //}
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return _esito;
        }

        private System.Boolean _verificaScadenziario_CODICEFISCALE(System.String _PathInput, ref List<System.IO.DirectoryInfo> _ListFolder, ref List<System.String> _ListErrore)
        {
            System.Boolean _esito = true;
            try
            {
                DirectoryInfo _DIR_CRIF_Work = new DirectoryInfo(Crif_Configuration.CRIF_Input_WORK);
                FileInfo _FileInfoCSV = new FileInfo(_PathInput);
                var oDevExpressExcel = new CRIF_KYC.DevExpressMdg.ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_PathInput, DevExpress.Spreadsheet.DocumentFormat.Csv);
                var _dtDati = oDevExpressExcel.ExportDataTable(0);

                if (_dtDati is null || _dtDati.Rows.Count == 0)
                {
                    log.Info($"Nessun Dato presente nel CSV selezionato : {_FileInfoCSV.Name}");
                    ///in questo caso significa che il CSV è vuoto e non è necessario proseguire con l'elaborazione  
                    //PopolaScarti(_FileInfoCSV);
                    _Crif_Warning.Add(new Crif_Warning()
                    {
                        Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                        Warning_Element = $"Nessun Dato presente nel CSV selezionato : {_FileInfoCSV.Name}"
                    });
                    _esito = false;
                }
                else
                {
                    foreach (System.Data.DataRow _Record in _dtDati.Rows)
                    {
                        //Verifico che la provenienza sia corretta
                        if (_Record["Provenienza"] == System.DBNull.Value)
                        {
                            _Crif_Warning.Add(new Crif_Warning()
                            {
                                Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente un valore di provenienza vuoto"
                            });
                            _esito = false;
                        }
                        else
                        {
                            System.String _Provenienza = _Record["Provenienza"].ToString();
                            if (!_Provenienza.ToUpper().Equals("C"))
                            {
                                _Crif_Warning.Add(new Crif_Warning()
                                {
                                    Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                    Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente un valore di provenienza non corretto : {_Provenienza}"
                                });
                                _esito = false;
                            }
                        }

                        //Estraggo il valore dell'taxCode presente sulla riga
                        System.String _TaxCode = _Record["taxCode"].ToString().ToUpper();
                        if (!string.IsNullOrEmpty(_TaxCode))
                        {
                            if (_DIR_CRIF_Work.GetDirectories($"*{_TaxCode}*").Length == 0)
                            {
                                _ListErrore.Add($"Cartella Non Trovata per Codice iscale {_TaxCode}");
                                _Crif_Warning.Add(new Crif_Warning()
                                {
                                    Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                                    Warning_Element = $"Nel file {_FileInfoCSV.Name} risulta presente il codice fiscale {_TaxCode}, ma non ha associato nessun archivio"
                                });
                                _esito = false;
                            }
                            //else
                            //{
                            //    var _directory = _DIR_CRIF_Work.GetDirectories($"*{_TaxCode}*").First();
                            //    if (_directory.Exists)
                            //    {
                            //        _ListErrore.Add($"Cartella Trovata con nome :{_directory.FullName}");
                            //        _ListFolder.Add(_directory);
                            //    }
                            //}
                        }

                        //Ho commentato questo pezzo in attesa che Federici ci faccia sapere come vuole muoversi in merito a questa verifica sul biometricCheckResult

                        ////effettuo la verifica inerente al fatto che il check Biometrico sia impostato correttamente a 1 se a 0 l'archivio deve essere spostato.
                        System.String _BiometricValue = _Record["biometricCheckResult"].ToString();
                        if (!string.IsNullOrEmpty(_BiometricValue) && _BiometricValue.Equals("0"))
                        {
                            var _directory = _DIR_CRIF_Work.GetDirectories($"*{_TaxCode}*").First();
                            if (_directory.Exists)
                            {
                                _ListErrore.Add($"Cartella Trovata con nome :{_directory.Name} per TaxCode {_TaxCode} e biometricCheckResult = 0");
                                _ListFolder.Add(_directory);
                            }
                            //non metto esito false perchè va comunque lavorate le altre cartelle presenti.
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return _esito;
        }

        private void _deleteFolderNotCorrect(List<System.IO.DirectoryInfo> _ListFolder, List<FileInfo> _ListScadenziario)
        {
            try
            {
                log.Info("Inizio _deleteFolderNotCorrect");
                if (_ListScadenziario.Count > 0)
                {
                    foreach (var _fileinfo in _ListScadenziario)
                    {
                        ///Copio in scarti la lista degli scadenziari non corretti.
                        PopolaScarti(_fileinfo);
                        //File.Delete(_fileinfo.FullName);
                        log.Info($"Cancellato il file {_fileinfo.FullName}");
                    }
                }

                if (_ListFolder.Count > 0)
                {
                    foreach (var _folderInfo in _ListFolder)
                    {
                        ///Prima della cancellazione effettuo una copia negli scarti delle cartelle non lavorate
                        PopolaScarti(_folderInfo);
                        //Directory.Delete(_folderInfo.FullName, true);
                        log.Info($"Cancellato la cartella {_folderInfo.FullName}");
                    }
                }
                log.Info("Fine _deleteFolderNotCorrect");
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Effettuo la Copia del file all'interno della Directory di lavoro
        /// </summary>
        /// <param name="_fileInfo"></param>
        public void CopiaFileInWork(FileInfo _fileInfo)
        {
            try
            {
                Crif_Zip _crif_Zip = new Crif_Zip();
                if (!Directory.Exists(Crif_Configuration.CRIF_Input_WORK)) { Directory.CreateDirectory(Crif_Configuration.CRIF_Input_WORK); }

                FileInfo _file = null;

                if (Configuration.Crif_Configuration.CRIF_CERTIFICATO_PGP_Enable)
                {
                    _file = new FileInfo(Path.GetFileNameWithoutExtension(_fileInfo.Name));
                }
                else
                {
                    _file = new FileInfo(_fileInfo.Name);
                }

                if (_file.Extension.ToUpper().Equals(".ZIP") || _file.Extension.ToUpper().Equals(".RAR"))
                {
                    log.Info($"Zip : {_file.Name} Estratto nella cartella {Crif_Configuration.CRIF_Input_WORK}");
                    _crif_Zip.extract_Zip(Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, _file.Name), Crif_Configuration.CRIF_Input_WORK);
                }
                else
                {
                    log.Info($"Documento : {_file.Name} Copiato nella cartella {Crif_Configuration.CRIF_Input_WORK}");
                    File.Copy(Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, _file.Name), Path.Combine(Crif_Configuration.CRIF_Input_WORK, _file.Name));
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteWorkFolder()
        {
            try
            {
                if (Directory.Exists(Crif_Configuration.CRIF_Input_WORK))
                {
                    if (!Directory.Exists(Crif_Configuration.CRIF_Input_WORK_BACKUP)) { Directory.CreateDirectory(Crif_Configuration.CRIF_Input_WORK_BACKUP); }
                    Crif_Zip _Crif_Zip = new Crif_Zip();
                    if (new DirectoryInfo(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY).Exists)
                    {
                        _Crif_Zip.Zip_Directory(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY, Crif_Configuration.CRIF_Input_WORK_BACKUP);
                    }
                    Directory.Delete(Crif_Configuration.CRIF_Input_WORK, true);
                }
                if (Directory.Exists(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED))
                {
                    if (!Directory.Exists(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED_BACKUP)) { Directory.CreateDirectory(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED_BACKUP); }
                    Crif_Zip _Crif_Zip = new Crif_Zip();
                    _Crif_Zip.Zip_Directory(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, Crif_Configuration.CRIF_Input_SFTP_DECRYPTED_BACKUP);
                    Directory.Delete(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, true);
                }
                ///aggiungo la pulitura delle cartelle dove carico a documentale quando vuote.

                
                var _dirOutputDocumentary = new DirectoryInfo(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY);
                
                if (_dirOutputDocumentary.Parent.Exists)
                {
                    ///Cancellazione directory vuote
                    DeleteEmptyDirectory(_dirOutputDocumentary.Parent.FullName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Funzione per effettuare la cancellazione delle cartelle vuote partende da un path di root iniziale
        /// </summary>
        /// <param name="startLocation"></param>
        private static void DeleteEmptyDirectory(string startLocation)
        {
            foreach (var directory in Directory.GetDirectories(startLocation))
            {                             
                DeleteEmptyDirectory(directory);               
                if (Directory.GetFiles(directory).Length == 0 &&  Directory.GetDirectories(directory).Length == 0)
                {               
                    Directory.Delete(directory, false);
                }
            }
        }

        /// <summary>
        /// Permette di spostare nella cartella Scarti tutti i dati lavorati 
        /// </summary>
        /// <param name="_fileinfo">File info contenente il file da lavorare</param>
        public void PopolaScarti(FileInfo _fileinfo)
        {
            try
            {
                if (!Directory.Exists(Crif_Configuration.CRIF_SCARTI)) { Directory.CreateDirectory(Crif_Configuration.CRIF_SCARTI); }

                File.Copy(_fileinfo.FullName, Path.Combine(Crif_Configuration.CRIF_SCARTI, _fileinfo.Name));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Popolo gli Scarti zippando la directory appena lavorata
        /// </summary>
        /// <param name="_Directoryinfo">Directory Info</param>
        public void PopolaScarti(DirectoryInfo _Directoryinfo)
        {
            try
            {
                if (!Directory.Exists(Crif_Configuration.CRIF_SCARTI)) { Directory.CreateDirectory(Crif_Configuration.CRIF_SCARTI); }
                Crif_Zip _Crif_Zip = new Crif_Zip();
                _Crif_Zip.Zip_Directory(_Directoryinfo.FullName, Crif_Configuration.CRIF_SCARTI, String.Concat(_Directoryinfo.Name, ".zip"));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Funzione che permette di popolare la cartella di KYC con gli archivi desiderati.
        /// </summary>
        /// <param name="_Directoryinfo">Directory Info</param>
        public void Popola_KYC(DirectoryInfo _Directoryinfo)
        {
            try
            {
                if (!Directory.Exists(Crif_Configuration.CRIF_REPORT_KYC)) { Directory.CreateDirectory(Crif_Configuration.CRIF_REPORT_KYC); }
                Crif_Zip _Crif_Zip = new Crif_Zip();
                _Crif_Zip.Zip_Directory(_Directoryinfo.FullName, Crif_Configuration.CRIF_REPORT_KYC, String.Concat(_Directoryinfo.Name, ".zip"));
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}

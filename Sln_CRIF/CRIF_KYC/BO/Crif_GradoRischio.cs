﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using CRIF_KYC.Configuration;
using System.Data;
using System.IO;
using System.Data.SqlClient;

namespace CRIF_KYC.GradoRischio
{
    class CRIF_GradoRischio
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public void export_GradoRischio(DataRow _KYC_SCADENZIARIO, DirectoryInfo _dirInfo, ref List<CRIF_GradoRischioItem> _Row_Indice, ref List<DirectoryInfo> _Row_Folder)
        {
            Int64 _codNDG = 0;
            System.String _CLASSE_CLIENTELA = string.Empty;
            System.String _TIPOLOGIA_CLIENTELA = string.Empty;
            System.String _NAZIONALITA = string.Empty;
            System.String _PROVINCIA = String.Empty;
            System.String _provScadenziario = string.Empty;
            Boolean _AltoRischio = false;
            CRIF_GradoRischioItem _GradoRischioItem = null;
            try
            {

                var _taxCode = _KYC_SCADENZIARIO["taxcode"].ToString().ToUpper();
                var _biometricCheckResult = _KYC_SCADENZIARIO["biometricCheckResult"].ToString();
                var _documentType = String.IsNullOrEmpty(_KYC_SCADENZIARIO["documentType"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["documentType"].ToString().ToUpper().Replace("'","''").Replace("À", "A");

                if (_biometricCheckResult.Equals("1"))
                {
                    log.Info($"Verifica Grado Rischio CODICE FISCALE : {_taxCode}");
                    if (!String.IsNullOrEmpty(_taxCode))
                    {
                        _GradoRischioItem = new CRIF_GradoRischioItem();

                        _codNDG = Gestione_Verifica_PD_ANAGRAFICA(_taxCode, ref _CLASSE_CLIENTELA, ref _TIPOLOGIA_CLIENTELA,ref _NAZIONALITA);
                        log.Info($"Verifica Grado Rischio NDG : {_codNDG}");
                        ///Verifico se il soggetto è ad alto o basso rischio                    
                        _AltoRischio = Gestione_Verifica_PD_ANAGRAFICA_CRIF(_codNDG);

                        ///verifico se è ad alto rischio e in quel caso aggiungo la cartella per relativa copia al KYC
                        if (_AltoRischio)
                        {
                            _Row_Folder.Add(_dirInfo);
                            ///Se alto rischio metto H
                            _GradoRischioItem.provenienza_record = "H";
                        }
                        else
                        {
                            //Se Basso rischio metto C
                            _GradoRischioItem.provenienza_record = "C";
                        }

                        _GradoRischioItem.nome = String.IsNullOrEmpty(_KYC_SCADENZIARIO["firstName"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["firstName"].ToString().ToUpper();
                        _GradoRischioItem.cognome = String.IsNullOrEmpty(_KYC_SCADENZIARIO["lastName"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["lastName"].ToString().ToUpper();
                        _GradoRischioItem.codice_fiscale = String.IsNullOrEmpty(_KYC_SCADENZIARIO["taxCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["taxCode"].ToString().ToUpper();
                        _GradoRischioItem.data_nascita = String.IsNullOrEmpty(_KYC_SCADENZIARIO["dateOfBirth"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["dateOfBirth"].ToString();
                        _GradoRischioItem.luogo_di_nascita = String.IsNullOrEmpty(_KYC_SCADENZIARIO["placeOfBirth"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["placeOfBirth"].ToString().ToUpper();
                        _GradoRischioItem.statodinascita = String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressNation"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressNation"].ToString().ToUpper();
                        //_GradoRischioItem.provincia_di_nascita = String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressNationCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressNationCode"].ToString();
                        _provScadenziario = String.IsNullOrEmpty(_KYC_SCADENZIARIO["placeOfBirth"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["placeOfBirth"].ToString();
                        _GradoRischioItem.provincia_di_nascita = Gestione_Provincia(_provScadenziario);
                        _GradoRischioItem.ndg = _codNDG.ToString();
                        _GradoRischioItem.classificazione = _CLASSE_CLIENTELA;
                        _GradoRischioItem.tipologia = _TIPOLOGIA_CLIENTELA;
                        _GradoRischioItem.sesso = String.IsNullOrEmpty(_KYC_SCADENZIARIO["gender"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["gender"].ToString().ToUpper();
                        //_GradoRischioItem.nazionalità = String.IsNullOrEmpty(_KYC_SCADENZIARIO["nationality"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["nationality"].ToString();
                        _GradoRischioItem.nazionalità = _NAZIONALITA;


                        _GradoRischioItem.seconda_cittadinanza = "";
                        if (_AltoRischio)
                        {
                            _GradoRischioItem.rischio_gianos = "4";
                        }
                        else
                        {
                            _GradoRischioItem.rischio_gianos = "2";
                        }
                        _GradoRischioItem.data_review = "";
                        _GradoRischioItem.tipo_documento = Gestione_TipoDocumento(_documentType);
                        _GradoRischioItem.numero_documento = String.IsNullOrEmpty(_KYC_SCADENZIARIO["documentNumber"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["documentNumber"].ToString();
                        _GradoRischioItem.data_rilascio = String.IsNullOrEmpty(_KYC_SCADENZIARIO["issuingDate"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["issuingDate"].ToString();
                        _GradoRischioItem.luogo_rilascio = String.IsNullOrEmpty(_KYC_SCADENZIARIO["issuingPlace"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["issuingPlace"].ToString().ToUpper();
                        _GradoRischioItem.provincia_rilascio = String.IsNullOrEmpty(_KYC_SCADENZIARIO["issuingProvince"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["issuingProvince"].ToString().ToUpper();
                        _GradoRischioItem.stato_rilascio = String.IsNullOrEmpty(_KYC_SCADENZIARIO["issuingCountry"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["issuingCountry"].ToString().ToUpper();
                        _GradoRischioItem.data_scadenza = String.IsNullOrEmpty(_KYC_SCADENZIARIO["expiryDate"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["expiryDate"].ToString();
                        _GradoRischioItem.tipo_di_attivita_economica = String.IsNullOrEmpty(_KYC_SCADENZIARIO["TAECode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["TAECode"].ToString();
                        _GradoRischioItem.attività_lavorativa = String.IsNullOrEmpty(_KYC_SCADENZIARIO["jobTitleCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["jobTitleCode"].ToString();
                        _GradoRischioItem.nazione_attività = String.IsNullOrEmpty(_KYC_SCADENZIARIO["workNation"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["workNation"].ToString().ToUpper() ;
                        _GradoRischioItem.provincia_di_lavoro = String.IsNullOrEmpty(_KYC_SCADENZIARIO["workProvince"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["workProvince"].ToString().ToUpper() ;
                        _GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_1 = String.IsNullOrEmpty(_KYC_SCADENZIARIO["busRelNation1"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["busRelNation1"].ToString().ToUpper();
                        _GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_2 = String.IsNullOrEmpty(_KYC_SCADENZIARIO["busRelNation2"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["busRelNation2"].ToString().ToUpper();
                        _GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_3 = String.IsNullOrEmpty(_KYC_SCADENZIARIO["busRelNation3"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["busRelNation3"].ToString().ToUpper();
                        _GradoRischioItem.forma_giuridica = "";
                        _GradoRischioItem.indirizzo =string.Concat(String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressStreet"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressStreet"].ToString()," ", String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressNumber"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressNumber"].ToString());
                        _GradoRischioItem.comune = String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressLocality"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressLocality"].ToString().ToUpper();
                        _provScadenziario = String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressProvince"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressProvince"].ToString().ToUpper();
                        _GradoRischioItem.provincia = Gestione_Provincia(_provScadenziario);                        
                        _GradoRischioItem.cap = String.IsNullOrEmpty(_KYC_SCADENZIARIO["zipCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["zipCode"].ToString();
                        _GradoRischioItem.stato = String.IsNullOrEmpty(_KYC_SCADENZIARIO["addressNationCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["addressNationCode"].ToString();
                        _GradoRischioItem.telefono_abitazione = "";
                        _GradoRischioItem.telefono_cellulare = String.IsNullOrEmpty(_KYC_SCADENZIARIO["mobilePhone"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["mobilePhone"].ToString();
                        _GradoRischioItem.telefono_ufficio = "";
                        _GradoRischioItem.indirizzo_mail = String.IsNullOrEmpty(_KYC_SCADENZIARIO["email"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["email"].ToString().ToUpper();
                        _GradoRischioItem.settore_lavorativo = String.IsNullOrEmpty(_KYC_SCADENZIARIO["workFieldCode"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["workFieldCode"].ToString().ToUpper();
                        _GradoRischioItem.pep = String.IsNullOrEmpty(_KYC_SCADENZIARIO["pep"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["pep"].ToString().ToUpper();
                        _GradoRischioItem.flag_dbeasy = "";
                        _GradoRischioItem.pan_mascherato = "";
                        _GradoRischioItem.data_scadenza_carta = "";
                        _GradoRischioItem.issuereason = "";
                        _GradoRischioItem.stato_carta = "";
                        _GradoRischioItem.reason_stato_carta = "";
                        _GradoRischioItem.data_variazione_stato = "";
                        _GradoRischioItem.tipo_carta = "";
                        _GradoRischioItem.plastic_type = "";
                        _GradoRischioItem.data_ultima_review_kyc = "";
                        _GradoRischioItem.codice_titolare = "";
                        _GradoRischioItem.centro_tipo_carta = "";
                        _GradoRischioItem.tipo_punzonatura = "";
                        _GradoRischioItem.tipo_titolare = "";
                        _GradoRischioItem.codice_iniziativa = "";
                        _GradoRischioItem.codice_azione = "";
                        _GradoRischioItem.codice_ente = "";
                        _GradoRischioItem.abi_di_addebito = "";
                        _GradoRischioItem.owner_1 = "";
                        _GradoRischioItem.owner_2 = "";
                        _GradoRischioItem.owner_3 = "";
                        _GradoRischioItem.companyid = "";
                        _GradoRischioItem.telefono_abitazione_1 = "";
                        _GradoRischioItem.telefono_cellulare_1 = String.IsNullOrEmpty(_KYC_SCADENZIARIO["mobilePhone"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["mobilePhone"].ToString();
                        _GradoRischioItem.telefono_ufficio_1 = "";
                        _GradoRischioItem.indirizzo_mail_1 = String.IsNullOrEmpty(_KYC_SCADENZIARIO["email"].ToString()) ? String.Empty : _KYC_SCADENZIARIO["email"].ToString().ToUpper();

                        _Row_Indice.Add(_GradoRischioItem);
                    }
                    else
                    {
                        log.Info("Codice Fiscale Non valorizzato correttamente");
                    }
                }
                else { log.Info("biometricCheckResult Non valorizzato correttamente = 0"); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// estraggo il codice NDG del Soggetto
        /// </summary>
        /// <param name="_taxCode">Codice fiscale del soggetto</param>
        /// <returns></returns>
        private Int64 Gestione_Verifica_PD_ANAGRAFICA(System.String _taxCode, ref System.String _CLASSE_CLIENTELA, ref System.String _TIPOLOGIA_CLIENTELA, ref System.String _NAZIONALITA)
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;
            Int64 _codNDG = 0;


            try
            {
                cn = new OracleConnection(Crif_Configuration.CONNECTION_ORACLE);
                cmd = new OracleCommand("select * from PD_ANAGRAFICA WHERE CODICE_FISCALE = :CODICE_FISCALE and TIPO_NOMINATIVO = '001'", cn);
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.Parameters.Add(new OracleParameter("CODICE_FISCALE", OracleDbType.NVarchar2, System.Data.ParameterDirection.Input));
                cmd.Parameters["CODICE_FISCALE"].Value = _taxCode;

                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {

                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE PREGRESSOPRE: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {
                    OracleDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            _codNDG = string.IsNullOrEmpty(reader["CODICE_ANAGRAFE"].ToString()) ? 0 : Convert.ToInt64(reader["CODICE_ANAGRAFE"].ToString());
                            _CLASSE_CLIENTELA = string.IsNullOrEmpty(reader["CLASSE_CLIENTELA"].ToString()) ? String.Empty : Convert.ToInt32(reader["CLASSE_CLIENTELA"].ToString()).ToString();
                            _TIPOLOGIA_CLIENTELA = string.IsNullOrEmpty(reader["TIPOLOGIA_CLIENTELA"].ToString()) ? String.Empty : Convert.ToInt32(reader["TIPOLOGIA_CLIENTELA"].ToString()).ToString();
                            _NAZIONALITA = string.IsNullOrEmpty(reader["NAZIONALITA"].ToString()) ? String.Empty : reader["NAZIONALITA"].ToString().ToUpper();
                        }
                    }
                    else { log.Info("Nessun Record da lavorare"); }
                }
            }
            catch (Exception ex)
            {

                log.Error($"ERRORE IN FASE DI INSERT per il CODICE_ANAGRAFE NUMERO:{_codNDG} " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }
            return _codNDG;
        }

        private Boolean Gestione_Verifica_PD_ANAGRAFICA_CRIF(System.Int64 _codNDG)
        {
            OracleConnection cn = null;
            OracleCommand cmd = null;
            Boolean _AltoRischio = false;
            try
            {
                cn = new OracleConnection(Crif_Configuration.CONNECTION_ORACLE);
                cmd = new OracleCommand("select * from PD_ANAGRAFICA_CRIF where codice_anagrafe = :codice_anagrafe and CRIF_DISABLED = 0", cn);
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.Parameters.Add(new OracleParameter("codice_anagrafe", OracleDbType.Int64, System.Data.ParameterDirection.Input));
                cmd.Parameters["codice_anagrafe"].Value = _codNDG;

                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {

                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE PREGRESSOPRE: " + ex.Message + "\r\n");
                    throw;
                }

                if (cn.State == System.Data.ConnectionState.Open)
                {
                    OracleDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        log.Info($"Soggetto ad alto rischio {_codNDG}");
                        _AltoRischio = true;
                    }
                    else { log.Info($"Soggetto NON ad alto rischio {_codNDG}"); }
                }
            }
            catch (Exception ex)
            {

                log.Error($"ERRORE IN FASE DI INSERT per il CODICE_ANAGRAFE NUMERO:{_codNDG} " + ex.Message + "\r\n");
                log.Error(ex.StackTrace);
            }
            finally
            {
                if (cn != null && cn.State == System.Data.ConnectionState.Open)
                {
                    cn.Close();
                }
            }
            return _AltoRischio;
        }

        /// <summary>
        /// Recupero il tipo di documento partendo dall'anagrafica fornita da deut nello scadenziario
        /// </summary>
        /// <param name="_valTipoDocumento"></param>
        /// <returns></returns>
        public System.String Gestione_TipoDocumento(System.String _valTipoDocumento)
        {

            SqlConnection cn;
            SqlCommand command;
            string sql = null;
            System.String _codiceDocumento = string.Empty;


            sql = $"SELECT * FROM TIPO_DOCUMENTO WHERE DESCRIZIONE = REPLACE(UPPER('{_valTipoDocumento}'),'''''','''')";

            cn = new SqlConnection(Crif_Configuration.CONNECTION_SQLSERVER);
            try
            {
                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {

                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE SQL SERVER: " + ex.Message + "\r\n");
                    throw;
                }
                if (cn.State == System.Data.ConnectionState.Open)
                {
                    command = new SqlCommand(sql, cn);
                    SqlDataReader _dr = command.ExecuteReader();
                    //_dr.GetString(0);
                    DataTable dtCustomers = new DataTable("TipoDoc");
                    dtCustomers.Load(_dr);
                    if (dtCustomers.Rows.Count > 0)
                    {
                        // command.Dispose();
                        _codiceDocumento = dtCustomers.Rows[0]["id"].ToString();
                    }
                    else
                    {
                        _codiceDocumento = "050";
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
              
            }
            return _codiceDocumento;
        }


        /// <summary>
        /// Funzione per estrarre il codice della provincia.
        /// </summary>
        /// <param name="_valComune">codice della provincia</param>
        /// <returns></returns>
        public System.String Gestione_Provincia(System.String _valComune)
        {

            SqlConnection cn;
            SqlCommand command;
            string sql = null;
            System.String _codiceSigla = string.Empty;


            ///sql = $"select Sigla from Province where UPPER(Nome) = UPPER(@Nome)";
            sql = $"select P.Sigla as Sigla from Province P INNER JOIN COMUNI C ON P.IDProvincia = C.IDProvincia where UPPER(C.Nome) = UPPER(@Nome)";

            cn = new SqlConnection(Crif_Configuration.CONNECTION_SQLSERVER);
            try
            {
                try
                {
                    cn.Open();
                }
                catch (Exception ex)
                {

                    log.Error("ERRORE IN FASE DI CONNESSIONE AL DATABASE SQL SERVER: " + ex.Message + "\r\n");
                    throw;
                }
                if (cn.State == System.Data.ConnectionState.Open)
                {
                    command = new SqlCommand(sql, cn);
                    SqlParameter _parametro = new SqlParameter("Nome", SqlDbType.VarChar);
                    _parametro.Value = _valComune;
                    command.Parameters.Add(_parametro);
                    SqlDataReader _dr = command.ExecuteReader();
                    //_dr.GetString(0);
                    DataTable dtCustomers = new DataTable("Province");
                    dtCustomers.Load(_dr);
                    if (dtCustomers.Rows.Count > 0)
                    {
                        // command.Dispose();
                        _codiceSigla = dtCustomers.Rows[0]["Sigla"].ToString();
                    }
                    else
                    {
                        _codiceSigla = String.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();

            }
            return _codiceSigla;
        }

    }



    /// <summary>
    /// Oggetto utilizzato per contenere le informazioni da girare a Bosetti
    /// </summary>
    class CRIF_GradoRischioItem
    {
        public System.String provenienza_record { get; set; }
        public System.String nome { get; set; }
        public System.String cognome { get; set; }
        public System.String codice_fiscale { get; set; }
        public System.String data_nascita { get; set; }
        public System.String luogo_di_nascita { get; set; }
        public System.String statodinascita { get; set; }
        public System.String provincia_di_nascita { get; set; }
        public System.String ndg { get; set; }
        public System.String classificazione { get; set; }
        public System.String tipologia { get; set; }
        public System.String sesso { get; set; }
        public System.String nazionalità { get; set; }
        public System.String seconda_cittadinanza { get; set; }
        public System.String rischio_gianos { get; set; }
        public System.String data_review { get; set; }
        public System.String tipo_documento { get; set; }
        public System.String numero_documento { get; set; }
        public System.String data_rilascio { get; set; }
        public System.String luogo_rilascio { get; set; }
        public System.String provincia_rilascio { get; set; }
        public System.String stato_rilascio { get; set; }
        public System.String data_scadenza { get; set; }
        public System.String tipo_di_attivita_economica { get; set; }
        public System.String attività_lavorativa { get; set; }
        public System.String nazione_attività { get; set; }
        public System.String provincia_di_lavoro { get; set; }
        public System.String paesi_con_cui_si_intrattengono_rapporti_di_business_1 { get; set; }
        public System.String paesi_con_cui_si_intrattengono_rapporti_di_business_2 { get; set; }
        public System.String paesi_con_cui_si_intrattengono_rapporti_di_business_3 { get; set; }
        public System.String forma_giuridica { get; set; }
        public System.String indirizzo { get; set; }
        public System.String comune { get; set; }
        public System.String provincia { get; set; }
        public System.String cap { get; set; }
        public System.String stato { get; set; }
        public System.String telefono_abitazione { get; set; }
        public System.String telefono_cellulare { get; set; }
        public System.String telefono_ufficio { get; set; }
        public System.String indirizzo_mail { get; set; }
        public System.String settore_lavorativo { get; set; }
        public System.String pep { get; set; }
        public System.String flag_dbeasy { get; set; }
        public System.String pan_mascherato { get; set; }
        public System.String data_scadenza_carta { get; set; }
        public System.String issuereason { get; set; }
        public System.String stato_carta { get; set; }
        public System.String reason_stato_carta { get; set; }
        public System.String data_variazione_stato { get; set; }
        public System.String tipo_carta { get; set; }
        public System.String plastic_type { get; set; }
        public System.String data_ultima_review_kyc { get; set; }
        public System.String codice_titolare { get; set; }
        public System.String centro_tipo_carta { get; set; }
        public System.String tipo_punzonatura { get; set; }
        public System.String tipo_titolare { get; set; }
        public System.String codice_iniziativa { get; set; }
        public System.String codice_azione { get; set; }
        public System.String codice_ente { get; set; }
        public System.String abi_di_addebito { get; set; }
        public System.String owner_1 { get; set; }
        public System.String owner_2 { get; set; }
        public System.String owner_3 { get; set; }
        public System.String companyid { get; set; }
        public System.String telefono_abitazione_1 { get; set; }
        public System.String telefono_cellulare_1 { get; set; }
        public System.String telefono_ufficio_1 { get; set; }
        public System.String indirizzo_mail_1 { get; set; }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.DevExpressMdg;
using System.IO;
using CRIF_KYC.Configuration;
using CRIF_KYC.GradoRischio;
using CRIF_KYC.Verifica;
using NLog.Fluent;

namespace CRIF_KYC.ExcelMDG
{
    class Crif_Excel
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        public System.Data.DataTable load_KYC_Scadenziario(System.String _pathExcel, System.Int16 _idSheet)
        {

            System.Data.DataTable _dtKYC_Scadenziario = null;
            try
            {
                ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                oDevExpressExcel.loadDocument(_pathExcel, DevExpress.Spreadsheet.DocumentFormat.Csv, ';'); ;
                _dtKYC_Scadenziario = oDevExpressExcel.ExportDataTable(_idSheet);

            }
            catch (Exception)
            {
                throw;
            }
            return _dtKYC_Scadenziario;
        }

        /// <summary>
        /// Genero il report per la produzione estraendo solo i soggetti che hanno come identificativo il grado di rischio alto
        /// </summary>
        /// <param name="_Scadenziario"></param>
        /// <param name="_LstIndice"></param>
        public void GenerateExcelCarteProd(FileInfo _Scadenziario, List<CRIF_GradoRischioItem> _LstIndice)
        {
            System.Data.DataTable _dtKYC_Scadenziario = null;
            System.String _DataReview = Path.GetFileNameWithoutExtension(_Scadenziario.Name.Replace("KYC_SCADENZIARIO_", String.Empty));
            String _nameFile = $"{System.DateTime.Now.ToString("yyyyMMdd")}_Review_date_CAMS.xlsx";
            System.String _PathExcel = Path.Combine(Crif_Configuration.CRIF_REPORT_PROD, _nameFile);
            CRIF_GradoRischioItem _Crif_gradorischioItem = null;
            try
            {
                if (!new FileInfo(_Scadenziario.FullName).Exists) { throw new Exception($"File scadenziario non presente {_Scadenziario.Name}"); }
                _dtKYC_Scadenziario = load_KYC_Scadenziario(_Scadenziario.FullName, 0);
                if (_dtKYC_Scadenziario != null && _dtKYC_Scadenziario.Rows.Count > 0)
                {
                    System.Data.DataTable _dtProduzione = new System.Data.DataTable();
                    _dtProduzione.Columns.Add("DATA_REVIEW", typeof(String));
                    _dtProduzione.Columns.Add("LASTNAME", typeof(String));
                    _dtProduzione.Columns.Add("FIRSTNAME", typeof(String));
                    _dtProduzione.Columns.Add("TAXCODE", typeof(String));

                    foreach (System.Data.DataRow _row in _dtKYC_Scadenziario.Rows)
                    {
                        _Crif_gradorischioItem = new CRIF_GradoRischioItem();

                        _Crif_gradorischioItem = _LstIndice.Where(x => x.codice_fiscale.Equals(_row["taxCode"].ToString())).FirstOrDefault(); ;

                        if (_Crif_gradorischioItem != null)
                        {
                            if (_Crif_gradorischioItem.rischio_gianos.Equals("2")) { 
                            var _rowProduzione = _dtProduzione.NewRow();
                            _rowProduzione["DATA_REVIEW"] = _DataReview;
                            _rowProduzione["LASTNAME"] = _row["lastName"] == null ? String.Empty : _row["lastName"].ToString();
                            _rowProduzione["FIRSTNAME"] = _row["firstName"] == null ? String.Empty : _row["firstName"].ToString();
                            _rowProduzione["TAXCODE"] = _row["taxCode"] == null ? String.Empty : _row["taxCode"].ToString();
                            _dtProduzione.Rows.Add(_rowProduzione);
                            }
                        }
                    }

                    if (_dtProduzione != null && _dtProduzione.Rows.Count > 0)
                    {
                        ExpressExcelMdg oDevExpressExcel = new ExpressExcelMdg();
                        oDevExpressExcel.RenameSheet(0, _DataReview);
                        oDevExpressExcel.AddTextInCell(_DataReview, _dtProduzione);
                        oDevExpressExcel.SaveDocument(_PathExcel);

                        //Se il file è creato correttamente effettuo il backup nella cartella preposta
                        if (new FileInfo(_PathExcel).Exists)
                        {
                            if (!Directory.Exists(Crif_Configuration.CRIF_REPORT_PROD_BACKUP)) { Directory.CreateDirectory(Crif_Configuration.CRIF_REPORT_PROD_BACKUP); }
                            File.Copy(_PathExcel, Path.Combine(Crif_Configuration.CRIF_REPORT_PROD_BACKUP, new FileInfo(_PathExcel).Name));
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void GenerateCSV_KYC(List<CRIF_GradoRischioItem> _ListIndice, List<DirectoryInfo> _listDirectory)
        {
            System.String _DataReview = System.DateTime.Now.ToString("yyyyMMdd");
            String _nameFileCSV = $"KYC_scadenziario_pd_{System.DateTime.Now.ToString("yyyyMMdd")}.csv";
            System.String _PathCSV = Path.Combine(Crif_Configuration.CRIF_REPORT_KYC, _nameFileCSV);

            try
            {
                if (!new DirectoryInfo(Crif_Configuration.CRIF_REPORT_KYC).Exists) { Directory.CreateDirectory(Crif_Configuration.CRIF_REPORT_KYC); }
                if (Configuration.Crif_Configuration.CRIF_ENABLE_REPORT_KYC)
                {
                    if (_ListIndice != null && _ListIndice.Count > 0)
                    {

                        System.Data.DataTable _dtProduzione = new System.Data.DataTable();
                        _dtProduzione.Columns.Add("provenienza_record", typeof(String));
                        _dtProduzione.Columns.Add("nome", typeof(String));
                        _dtProduzione.Columns.Add("cognome", typeof(String));
                        _dtProduzione.Columns.Add("codice_fiscale", typeof(String));
                        _dtProduzione.Columns.Add("data_nascita", typeof(String));
                        _dtProduzione.Columns.Add("luogo_di_nascita", typeof(String));
                        _dtProduzione.Columns.Add("statodinascita", typeof(String));
                        _dtProduzione.Columns.Add("provincia_di_nascita", typeof(String));
                        _dtProduzione.Columns.Add("ndg", typeof(String));
                        _dtProduzione.Columns.Add("classificazione", typeof(String));
                        _dtProduzione.Columns.Add("tipologia", typeof(String));
                        _dtProduzione.Columns.Add("sesso", typeof(String));
                        _dtProduzione.Columns.Add("nazionalità", typeof(String));
                        _dtProduzione.Columns.Add("seconda_cittadinanza", typeof(String));
                        _dtProduzione.Columns.Add("rischio_gianos", typeof(String));
                        _dtProduzione.Columns.Add("data_review", typeof(String));
                        _dtProduzione.Columns.Add("tipo_documento", typeof(String));
                        _dtProduzione.Columns.Add("numero_documento", typeof(String));
                        _dtProduzione.Columns.Add("data_rilascio", typeof(String));
                        _dtProduzione.Columns.Add("luogo_rilascio", typeof(String));
                        _dtProduzione.Columns.Add("provincia_rilascio", typeof(String));
                        _dtProduzione.Columns.Add("stato_rilascio", typeof(String));
                        _dtProduzione.Columns.Add("data_scadenza", typeof(String));
                        _dtProduzione.Columns.Add("tipo_di_attivita_economica", typeof(String));
                        _dtProduzione.Columns.Add("attività_lavorativa", typeof(String));
                        _dtProduzione.Columns.Add("nazione_attività", typeof(String));
                        _dtProduzione.Columns.Add("provincia_di_lavoro", typeof(String));
                        _dtProduzione.Columns.Add("paesi_con_cui_si_intrattengono_rapporti_di_business_1", typeof(String));
                        _dtProduzione.Columns.Add("paesi_con_cui_si_intrattengono_rapporti_di_business_2", typeof(String));
                        _dtProduzione.Columns.Add("paesi_con_cui_si_intrattengono_rapporti_di_business_3", typeof(String));
                        _dtProduzione.Columns.Add("forma_giuridica", typeof(String));
                        _dtProduzione.Columns.Add("indirizzo", typeof(String));
                        _dtProduzione.Columns.Add("comune", typeof(String));
                        _dtProduzione.Columns.Add("provincia", typeof(String));
                        _dtProduzione.Columns.Add("cap", typeof(String));
                        _dtProduzione.Columns.Add("stato", typeof(String));
                        _dtProduzione.Columns.Add("telefono_abitazione", typeof(String));
                        _dtProduzione.Columns.Add("telefono_cellulare", typeof(String));
                        _dtProduzione.Columns.Add("telefono_ufficio", typeof(String));
                        _dtProduzione.Columns.Add("indirizzo_mail", typeof(String));
                        _dtProduzione.Columns.Add("settore_lavorativo", typeof(String));
                        _dtProduzione.Columns.Add("pep", typeof(String));
                        _dtProduzione.Columns.Add("flag_dbeasy", typeof(String));
                        _dtProduzione.Columns.Add("pan_mascherato", typeof(String));
                        _dtProduzione.Columns.Add("data_scadenza_carta", typeof(String));
                        _dtProduzione.Columns.Add("issuereason", typeof(String));
                        _dtProduzione.Columns.Add("stato_carta", typeof(String));
                        _dtProduzione.Columns.Add("reason_stato_carta", typeof(String));
                        _dtProduzione.Columns.Add("data_variazione_stato", typeof(String));
                        _dtProduzione.Columns.Add("tipo_carta", typeof(String));
                        _dtProduzione.Columns.Add("plastic_type", typeof(String));
                        _dtProduzione.Columns.Add("data_ultima_review_kyc", typeof(String));
                        _dtProduzione.Columns.Add("codice_titolare", typeof(String));
                        _dtProduzione.Columns.Add("centro_tipo_carta", typeof(String));
                        _dtProduzione.Columns.Add("tipo_punzonatura", typeof(String));
                        _dtProduzione.Columns.Add("tipo_titolare", typeof(String));
                        _dtProduzione.Columns.Add("codice_iniziativa", typeof(String));
                        _dtProduzione.Columns.Add("codice_azione", typeof(String));
                        _dtProduzione.Columns.Add("codice_ente", typeof(String));
                        _dtProduzione.Columns.Add("abi_di_addebito", typeof(String));
                        _dtProduzione.Columns.Add("owner_1", typeof(String));
                        _dtProduzione.Columns.Add("owner_2", typeof(String));
                        _dtProduzione.Columns.Add("owner_3", typeof(String));
                        _dtProduzione.Columns.Add("companyid", typeof(String));
                        _dtProduzione.Columns.Add("telefono_abitazione_1", typeof(String));
                        _dtProduzione.Columns.Add("telefono_cellulare_1", typeof(String));
                        _dtProduzione.Columns.Add("telefono_ufficio_1", typeof(String));
                        _dtProduzione.Columns.Add("indirizzo_mail_1", typeof(String));

                        foreach (CRIF_GradoRischioItem _CRIF_GradoRischioItem in _ListIndice)
                        {
                            var _rowProduzione = _dtProduzione.NewRow();

                            _rowProduzione["provenienza_record"] = _CRIF_GradoRischioItem.provenienza_record;
                            _rowProduzione["nome"] = _CRIF_GradoRischioItem.nome;
                            _rowProduzione["cognome"] = _CRIF_GradoRischioItem.cognome;
                            _rowProduzione["codice_fiscale"] = _CRIF_GradoRischioItem.codice_fiscale;
                            _rowProduzione["data_nascita"] = _CRIF_GradoRischioItem.data_nascita;
                            _rowProduzione["luogo_di_nascita"] = _CRIF_GradoRischioItem.luogo_di_nascita;
                            _rowProduzione["statodinascita"] = _CRIF_GradoRischioItem.statodinascita;
                            _rowProduzione["provincia_di_nascita"] = _CRIF_GradoRischioItem.provincia_di_nascita;
                            _rowProduzione["ndg"] = _CRIF_GradoRischioItem.ndg;
                            _rowProduzione["classificazione"] = _CRIF_GradoRischioItem.classificazione;
                            _rowProduzione["tipologia"] = _CRIF_GradoRischioItem.tipologia;
                            _rowProduzione["sesso"] = _CRIF_GradoRischioItem.sesso;
                            _rowProduzione["nazionalità"] = _CRIF_GradoRischioItem.nazionalità;
                            _rowProduzione["seconda_cittadinanza"] = _CRIF_GradoRischioItem.seconda_cittadinanza;
                            _rowProduzione["rischio_gianos"] = _CRIF_GradoRischioItem.rischio_gianos;
                            _rowProduzione["data_review"] = _CRIF_GradoRischioItem.data_review;
                            _rowProduzione["tipo_documento"] = _CRIF_GradoRischioItem.tipo_documento;
                            _rowProduzione["numero_documento"] = _CRIF_GradoRischioItem.numero_documento;
                            _rowProduzione["data_rilascio"] = _CRIF_GradoRischioItem.data_rilascio;
                            _rowProduzione["luogo_rilascio"] = _CRIF_GradoRischioItem.luogo_rilascio;
                            _rowProduzione["provincia_rilascio"] = _CRIF_GradoRischioItem.provincia_rilascio;
                            _rowProduzione["stato_rilascio"] = _CRIF_GradoRischioItem.stato_rilascio;
                            _rowProduzione["data_scadenza"] = _CRIF_GradoRischioItem.data_scadenza;
                            _rowProduzione["tipo_di_attivita_economica"] = _CRIF_GradoRischioItem.tipo_di_attivita_economica;
                            _rowProduzione["attività_lavorativa"] = _CRIF_GradoRischioItem.attività_lavorativa;
                            _rowProduzione["nazione_attività"] = _CRIF_GradoRischioItem.nazione_attività;
                            _rowProduzione["provincia_di_lavoro"] = _CRIF_GradoRischioItem.provincia_di_lavoro;
                            _rowProduzione["paesi_con_cui_si_intrattengono_rapporti_di_business_1"] = _CRIF_GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_1;
                            _rowProduzione["paesi_con_cui_si_intrattengono_rapporti_di_business_2"] = _CRIF_GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_2;
                            _rowProduzione["paesi_con_cui_si_intrattengono_rapporti_di_business_3"] = _CRIF_GradoRischioItem.paesi_con_cui_si_intrattengono_rapporti_di_business_3;
                            _rowProduzione["forma_giuridica"] = _CRIF_GradoRischioItem.forma_giuridica;
                            _rowProduzione["indirizzo"] = _CRIF_GradoRischioItem.indirizzo;
                            _rowProduzione["comune"] = _CRIF_GradoRischioItem.comune;
                            _rowProduzione["provincia"] = _CRIF_GradoRischioItem.provincia;
                            _rowProduzione["cap"] = _CRIF_GradoRischioItem.cap;
                            _rowProduzione["stato"] = _CRIF_GradoRischioItem.stato;
                            _rowProduzione["telefono_abitazione"] = _CRIF_GradoRischioItem.telefono_abitazione;
                            _rowProduzione["telefono_cellulare"] = _CRIF_GradoRischioItem.telefono_cellulare;
                            _rowProduzione["telefono_ufficio"] = _CRIF_GradoRischioItem.telefono_ufficio;
                            _rowProduzione["indirizzo_mail"] = _CRIF_GradoRischioItem.indirizzo_mail;
                            _rowProduzione["settore_lavorativo"] = _CRIF_GradoRischioItem.settore_lavorativo;
                            _rowProduzione["pep"] = _CRIF_GradoRischioItem.pep;
                            _rowProduzione["flag_dbeasy"] = _CRIF_GradoRischioItem.flag_dbeasy;
                            _rowProduzione["pan_mascherato"] = _CRIF_GradoRischioItem.pan_mascherato;
                            _rowProduzione["data_scadenza_carta"] = _CRIF_GradoRischioItem.data_scadenza_carta;
                            _rowProduzione["issuereason"] = _CRIF_GradoRischioItem.issuereason;
                            _rowProduzione["stato_carta"] = _CRIF_GradoRischioItem.stato_carta;
                            _rowProduzione["reason_stato_carta"] = _CRIF_GradoRischioItem.reason_stato_carta;
                            _rowProduzione["data_variazione_stato"] = _CRIF_GradoRischioItem.data_variazione_stato;
                            _rowProduzione["tipo_carta"] = _CRIF_GradoRischioItem.tipo_carta;
                            _rowProduzione["plastic_type"] = _CRIF_GradoRischioItem.plastic_type;
                            _rowProduzione["data_ultima_review_kyc"] = _CRIF_GradoRischioItem.data_ultima_review_kyc;
                            _rowProduzione["codice_titolare"] = _CRIF_GradoRischioItem.codice_titolare;
                            _rowProduzione["centro_tipo_carta"] = _CRIF_GradoRischioItem.centro_tipo_carta;
                            _rowProduzione["tipo_punzonatura"] = _CRIF_GradoRischioItem.tipo_punzonatura;
                            _rowProduzione["tipo_titolare"] = _CRIF_GradoRischioItem.tipo_titolare;
                            _rowProduzione["codice_iniziativa"] = _CRIF_GradoRischioItem.codice_iniziativa;
                            _rowProduzione["codice_azione"] = _CRIF_GradoRischioItem.codice_azione;
                            _rowProduzione["codice_ente"] = _CRIF_GradoRischioItem.codice_ente;
                            _rowProduzione["abi_di_addebito"] = _CRIF_GradoRischioItem.abi_di_addebito;
                            _rowProduzione["owner_1"] = _CRIF_GradoRischioItem.owner_1;
                            _rowProduzione["owner_2"] = _CRIF_GradoRischioItem.owner_2;
                            _rowProduzione["owner_3"] = _CRIF_GradoRischioItem.owner_3;
                            _rowProduzione["companyid"] = _CRIF_GradoRischioItem.companyid;
                            _rowProduzione["telefono_abitazione_1"] = _CRIF_GradoRischioItem.telefono_abitazione_1;
                            _rowProduzione["telefono_cellulare_1"] = _CRIF_GradoRischioItem.telefono_cellulare_1;
                            _rowProduzione["telefono_ufficio_1"] = _CRIF_GradoRischioItem.telefono_ufficio_1;
                            _rowProduzione["indirizzo_mail_1"] = _CRIF_GradoRischioItem.indirizzo_mail_1;
                            _dtProduzione.Rows.Add(_rowProduzione);
                        }

                        ExpressExcelMdg oDevExpressCSV = new ExpressExcelMdg();
                        oDevExpressCSV.RenameSheet(0, _DataReview);
                        oDevExpressCSV.AddTextInCell(_DataReview, _dtProduzione, false);
                        oDevExpressCSV.SaveDocumentCSV(_PathCSV, ';');


                        //effettuo il caricamento delle cartelle al KYC zippandole
                        if (_listDirectory.Count > 0)
                        {

                            ///Prendo tutte le cartelle e successivamente effettuo uno zip e le mando a 
                            foreach (DirectoryInfo _dir in _listDirectory)
                            {

                                Crif_Verifica _Crif_Verifica = new Crif_Verifica();
                                _Crif_Verifica.Popola_KYC(_dir);
                                log.Info($"Cartelle Depositate al KYC : Nome => {_dir.Name}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRIF_KYC.Errore
{
    class Crif_Errori
    {
        public Boolean Errore { get; set; }
        public List<string> Errore_List { get; set; }
    }
}

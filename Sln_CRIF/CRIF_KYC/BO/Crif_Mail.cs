﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.Configuration;
using CRIF_KYC.Errore;

namespace CRIF_KYC.Mail
{
    public class Crif_Mail
    {
        public System.String email_smtpserver { get; set; }
        public System.Boolean email_sendemail { get; set; }
        public System.String email_from { get; set; }
        public System.String email_to { get; set; }
        public System.String email_cc { get; set; }
        public System.String email_eccezione_to { get; set; }
        public System.String email_path_folder { get; set; }
        public System.String email_name_file { get; set; }
        public List<System.String> email_Attachement { get; set; }

        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();


        public void SendMailLog()
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                message = new MailMessage(this.email_from, this.email_to);
                message.Subject = $" DEUT - CRIF_KYC - SPORTELLI-CARTE - Execution Log ";
                message.Body = preparataTestoMailLog().ToString();

                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;

                if (email_Attachement.Count > 0)
                {
                    foreach (var _path in email_Attachement)
                    {
                        message.Attachments.Add(new Attachment(_path));
                    }
                }
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }

        /// <summary>
        /// Invio della mail contente errori
        /// </summary>
        /// <param name="ex">Eccezione</param>
        /// <param name="_microframe">Abilito l'invio a Microframe dell'eccezione</param>
        public void SendMailError(Boolean _microframe)
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                if (_microframe)
                {
                    message = new MailMessage(this.email_from, this.email_to);
                    message.CC.Add(email_cc);
                }
                else
                {
                    message = new MailMessage(this.email_from, this.email_eccezione_to);
                }
                message.Subject = $" Deutsche Bank - CRIF_KYC ";
                if (_microframe)
                {
                    message.Body = preparataTestoMail().ToString();
                }
                else
                {
                    message.Body = preparataTestoMailStackTrace().ToString();
                }
                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;
                if (_microframe)
                {
                    if (email_Attachement.Count > 0)
                    {
                        foreach (var _path in email_Attachement)
                        {
                            message.Attachments.Add(new Attachment(_path));
                        }
                    }
                }

                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }



        /// <summary>
        /// Mail generica di Alert
        /// </summary>
        /// <param name="_microframe">Abilita L'invio a Microframe della segnalazione</param>
        /// <param name="_TestoMail">Testo della Mail</param>
        public void SendMail(Boolean _microframe, System.String _TestoMail)
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                if (_microframe)
                {
                    message = new MailMessage(this.email_from, this.email_to);
                    message.CC.Add(email_cc);
                }
                else
                {
                    message = new MailMessage(this.email_from, this.email_eccezione_to);
                }
                message.Subject = $"Deutsche Bank - CRIF_KYC";

                message.Body = preparataTestoMail(_TestoMail).ToString();

                client = new SmtpClient(this.email_smtpserver);
                client.UseDefaultCredentials = true;
                client.Send(message);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }


        public void SendMailWarning(System.String _NomeApplicazione, List<System.String> _TestoMail)
        {
            MailMessage message = null;
            SmtpClient client = null;
            try
            {
                if (this.email_sendemail)
                {
                    message = new MailMessage(this.email_from, this.email_to);
                    if (!string.IsNullOrEmpty(email_cc))
                    {
                        message.CC.Add(email_cc);
                    }
                    message.Subject = $"Deutsche Bank - CRIF_KYC - {_NomeApplicazione} ";
                    message.Body = preparataTestoMailWarning(_TestoMail).ToString();
                    client = new SmtpClient(this.email_smtpserver);
                    client.UseDefaultCredentials = true;
                    client.Send(message);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (message != null) { message.Dispose(); }
                if (client != null) { client.Dispose(); }
            }
        }

        //public void SendMailAttachment()
        //{
        //    MailMessage message = null;
        //    SmtpClient client = null;
        //    try
        //    {
        //        message = new MailMessage(this.email_from, this.email_to);
        //        message.CC.Add(email_cc);                
        //        message.Subject = $"Eurovita Report Fatturazione : {this.email_name_file}";
        //        message.Body = $"Buongiorno,{Environment.NewLine}In allegato alla presente trovare il report di fatturazione di Eurovita.{Environment.NewLine}Grazie.";
        //        message.Attachments.Add(new Attachment(System.IO.Path.Combine(this.email_path_folder,this.email_name_file)));
        //        client = new SmtpClient(this.email_smtpserver);
        //        client.UseDefaultCredentials = true;
        //        client.Send(message);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (message != null) { message.Dispose(); }
        //        if (client != null) { client.Dispose(); }
        //    }
        //}

        private System.Text.StringBuilder preparataTestoMail()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: CRIF_KYC{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"{System.Environment.NewLine}Elaborazione terminata con Errori.");
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host :{Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMailLog()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: CRIF_KYC{System.Environment.NewLine} Referente : Soregaroli C.");                
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host :{Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMail(System.String _Messaggio)
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset Di Riferimento: CRIF_KYC{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"Alert Generated : {_Messaggio}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }

        private System.Text.StringBuilder preparataTestoMailWarning(List<System.String> _Messaggio)
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"Buongiorno,");
                strMail.Append($"{Environment.NewLine}la presente per segnalare le seguenti anomalie nell'elaborazione del giorno {System.DateTime.Now.ToShortDateString().ToString()} : ");
                foreach (var _stringa in _Messaggio)
                {
                    strMail.Append($"{Environment.NewLine}{_stringa}{Environment.NewLine}");
                }

                strMail.Append($"{Environment.NewLine}Cordiali Saluti.");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }



        private System.Text.StringBuilder preparataTestoMailStackTrace()
        {
            StringBuilder strMail = new StringBuilder();
            try
            {
                strMail.Append($"{System.Environment.NewLine}Asset di Riferimento: CRIF_KYC{System.Environment.NewLine} Referente : Soregaroli C.");
                strMail.Append($"{System.Environment.NewLine}Elaborazione terminata con Errori.");
                strMail.Append($"{System.Environment.NewLine}Verificare il Log presente sull'Host : {Environment.MachineName}");
            }
            catch (Exception)
            {
                throw;
            }
            return strMail;
        }
        /// <summary>
        /// Metodo per salvare in un file l'elenco degli errori
        /// </summary>
        /// <param name="_crif_Errori"></param>
        /// <returns></returns>
        public System.String CreateLogfile(List<System.String> Errore_List)
        {

            System.String _logfile = Path.Combine(Crif_Configuration.CRIF_LOG_ERROR, $"Log_File_Error_{System.DateTime.Now.Millisecond}.txt");
            try
            {
                if (!Directory.Exists(Crif_Configuration.CRIF_LOG_ERROR)) { Directory.CreateDirectory(Crif_Configuration.CRIF_LOG_ERROR); }
                else
                {
                    var _dirInfo = new DirectoryInfo(Crif_Configuration.CRIF_LOG_ERROR);
                    foreach (System.IO.FileInfo file in _dirInfo.GetFiles()) file.Delete();
                }

                if (!File.Exists(_logfile))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(_logfile))
                    {
                        sw.WriteLine(string.Join(Environment.NewLine, Errore_List.ToArray()));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _logfile;
        }


    }
}

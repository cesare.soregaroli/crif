﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CRIF_KYC.ExcelMDG;
using CRIF_KYC.Zip;
using CRIF_KYC.Configuration;
using System.Globalization;
using CRIF_KYC.Warning;
using CRIF_KYC.GradoRischio;
using System.Linq.Expressions;

namespace CRIF_KYC.Lavorazione
{
    public class Crif_Lavorazione
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();

        public List<Crif_Warning> _Crif_Warning { get; set; }
        /// <summary>
        /// Metodo per effettuare la lavorazione dei file presenti
        /// </summary>      
        public void workExtractZipFile()
        {
            try
            {
                //Verifico la presenza della cartella di lavoro
                DirectoryInfo _DIR_CRIF_Work = new DirectoryInfo(Crif_Configuration.CRIF_Input_WORK);
                IEnumerable<FileInfo> _KYC_SCADENZIARIO = _DIR_CRIF_Work.GetFiles().Where(x => x.Name.ToUpper().Contains("KYC_SCADENZIARIO") && x.Extension.ToUpper().Equals(".CSV"));
                if (_KYC_SCADENZIARIO == null) { log.Error($"File KYC_SCADENZIARIO non presente nella cartella: {_DIR_CRIF_Work}"); throw new Exception($"File KYC_SCADENZIARIO non presente nella cartella: {_DIR_CRIF_Work}"); }

                Crif_Excel oCrif_Excel = new Crif_Excel();
                foreach (FileInfo _fileInfo in _KYC_SCADENZIARIO)
                {
                    log.Info($"Inizio Elaborazione file scadenziario : {_fileInfo.Name}");
                    Crif_Configuration.CRIF_INDICE_DOCUMENTARY = Path.Combine(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY,
                        string.Format(System.Configuration.ConfigurationManager.AppSettings["CRIF_INDICE_DOCUMENTARY"].ToString(),
                        string.Concat(Crif_Configuration.CRIF_Tipo_Esecuzione, "_", Path.GetFileNameWithoutExtension(_fileInfo.Name))));
                    //FileInfo _fileInfo = _KYC_SCADENZIARIO.FirstOrDefault();
                    var source_KYC_SCADENZIARIO = oCrif_Excel.load_KYC_Scadenziario(_fileInfo.FullName, 0);
                    if (source_KYC_SCADENZIARIO == null || source_KYC_SCADENZIARIO.Rows.Count == 0) { log.Error($"File KYC_SCADENZIARIO non presente nella cartella / Vuoto: {_fileInfo.FullName}"); }
                    else
                    {

                        List<CRIF_GradoRischioItem> _Row_Indice = new List<CRIF_GradoRischioItem> { };
                        List<DirectoryInfo> _Row_Folder = new List<DirectoryInfo> { };
                        foreach (System.Data.DataRow _dr_KYC_SCADENZIARIO in source_KYC_SCADENZIARIO.Rows)
                        {
                            workNDGExtractZipFile(_DIR_CRIF_Work.FullName, _dr_KYC_SCADENZIARIO, ref _Row_Indice, ref _Row_Folder);
                        }

                        ///Deposito per KYC i file relativi ALto e BAsso Rischio in un CSV con relative cartelle da Verificare
                        Crif_Excel _crif_Excel = new Crif_Excel();
                        _crif_Excel.GenerateCSV_KYC(_Row_Indice, _Row_Folder);

                        //Metodo per Creare il report da dare alla produzione.
                        CreReportProdCarte(_fileInfo, _Row_Indice);

                        log.Info($"Invio File complessivo a VARSAVIA");
                        SendFileToVarsavia(_fileInfo.FullName);
                    }
                    //Cancello il file rar e tutte le cartelle ad esso legate
                    log.Info($"Cancello il File Appena Lavorato");
                    Delete_File_ToWork(_fileInfo.FullName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Metodo per effettuare la creazione dello scadenziario per la produzione
        /// </summary>
        /// <param name="_fileInfoScadenziario"></param>
        private static void CreReportProdCarte(FileInfo _fileInfoScadenziario, List<CRIF_GradoRischioItem> _LstIndice)
        {
            try
            {
                if (Crif_Configuration.CRIF_ENABLE_REPORT_PROD)
                {
                    Crif_Excel ocrif_Excel = new Crif_Excel();
                    //FileInfo _fileinfo = new FileInfo(@"C:\Progetti\Locali_Dati\CRIF\Programma\provascad\KYC_SCADENZIARIO_20200415090025487.csv");

                    ocrif_Excel.GenerateExcelCarteProd(_fileInfoScadenziario, _LstIndice);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Metodo per l'estrazione della cartella di lavoro
        /// </summary>
        /// <param name="_pathZip">Path contenete lo Zip da lavorare</param>
        /// <param name="_dr_KYC_SCADENZIARIO">Ndg ricercato</param>
        private void workNDGExtractZipFile(System.String _pathZip,
                                            System.Data.DataRow _dr_KYC_SCADENZIARIO,
                                            ref List<CRIF_GradoRischioItem> _Row_Indice,
                                            ref List<DirectoryInfo> _Row_Folder)
        {
            // Crif_Zip _Crif_zip = null;
            List<System.String> _lstIndice = new List<string> { };
            try
            {
                if (Crif_Configuration.CRIF_Tipo_Esecuzione.Equals("SPORTELLI"))
                {
                    var _taxCode = _dr_KYC_SCADENZIARIO["NDG"].ToString();

                    var _ndg = Decimal.Parse(_dr_KYC_SCADENZIARIO["NDG"].ToString(), System.Globalization.NumberStyles.Any);
                    log.Info($"NDG da CSV {_dr_KYC_SCADENZIARIO["NDG"].ToString()} ndg Sistemato {_ndg.ToString()}");

                    var _DirPathZip = new DirectoryInfo(_pathZip);


                    //Estraggo
                    var _directoryInput = new DirectoryInfo(Configuration.Crif_Configuration.CRIF_Input_WORK);
                    //var _DirectoryWorkNDG = _directoryInput.GetDirectories($"*{_ndg}*").First();
                    var _ListDirectoryWorkNDG = _directoryInput.GetDirectories($"*{_ndg}*").ToList();

                    foreach (var _DirectoryWorkNDG in _ListDirectoryWorkNDG)
                    {
                        //Estraggo il file CSV relativo alla posizione NDG lavorata
                        IEnumerable<FileInfo> _Microdata_NDG_csv = _DirectoryWorkNDG.GetFiles().Where(x => x.Name.ToUpper().Contains(_ndg.ToString()) && x.Extension.ToUpper().Equals(".CSV"));
                        if (_Microdata_NDG_csv == null) { log.Error($"File Microdata_NDG {_ndg.ToString()} non presente nella cartella: {_DirectoryWorkNDG}"); throw new Exception($"File Microdata_NDG {_ndg.ToString()} non presente nella cartella: {_DirectoryWorkNDG}"); }
                        FileInfo _fileInfoNDG = _Microdata_NDG_csv.FirstOrDefault();
                        Crif_Excel oCrif_ExcelNDG = new Crif_Excel();
                        var source_KYC_NDG = oCrif_ExcelNDG.load_KYC_Scadenziario(_fileInfoNDG.FullName, 0);
                        var folder_name = Path.GetFileNameWithoutExtension(_fileInfoNDG.Name);
                        var path_DocumentaryFolder = Path.Combine(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY, folder_name);

                        if (!Directory.Exists(path_DocumentaryFolder)) { Directory.CreateDirectory(path_DocumentaryFolder); }

                        foreach (System.Data.DataRow _dr_KYC_NDG in source_KYC_NDG.Rows)
                        {
                            var _sottoTipoDocumento = _dr_KYC_NDG["SOTTOTIPO_DOCUMENTO"].ToString();
                            var _tipoDocumento = _dr_KYC_NDG["TIPO_DOCUMENTO"].ToString();

                            var _tipodocIndice = _tipoDocumento.Equals("CRIF_REP") ? "KYC_GIANOS" : String.Empty;
                            var _filename = _dr_KYC_NDG["FILENAME"].ToString();
                            var txtIndice = workFileTodocumentary(_dr_KYC_SCADENZIARIO, _dr_KYC_NDG, folder_name, _filename, _tipodocIndice);
                            _lstIndice.Add(txtIndice);
                            //Estraggo il sottotipo del documento in maniera da applicare la regola inerente 
                            // Se K 1 solo file - se kf 2 copie se kfm

                            if (_tipoDocumento.Equals("CRIF_REP"))
                            {
                                if (_sottoTipoDocumento.Equals("KF") || _sottoTipoDocumento.Equals("KM"))
                                {
                                    if (_sottoTipoDocumento.Contains("F")) { _tipodocIndice = "FATCA"; } else if (_sottoTipoDocumento.Contains("M")) { _tipodocIndice = "PRIV"; }
                                    txtIndice = workFileTodocumentary(_dr_KYC_SCADENZIARIO, _dr_KYC_NDG, folder_name, String.Concat("M_", _filename), _tipodocIndice);
                                    _lstIndice.Add(txtIndice);
                                }
                                else if (_sottoTipoDocumento.Equals("KFM"))
                                {
                                    txtIndice = workFileTodocumentary(_dr_KYC_SCADENZIARIO, _dr_KYC_NDG, folder_name, String.Concat("F_", _filename), "FATCA");
                                    _lstIndice.Add(txtIndice);
                                    txtIndice = workFileTodocumentary(_dr_KYC_SCADENZIARIO, _dr_KYC_NDG, folder_name, String.Concat("M_", _filename), "PRIV");
                                    _lstIndice.Add(txtIndice);
                                }
                            }
                        }

                    }
                }
                else if (Crif_Configuration.CRIF_Tipo_Esecuzione.Equals("CARTE"))
                {
                    var _taxCode = _dr_KYC_SCADENZIARIO["taxcode"].ToString().ToUpper();
                    var _biometricCheckResult = _dr_KYC_SCADENZIARIO["biometricCheckResult"].ToString();

                    log.Info($"Codice Fiscale da CSV {_taxCode} ndg Sistemato");

                    if (_biometricCheckResult.Equals("1"))
                    {
                        var _DirPathZip = new DirectoryInfo(_pathZip);


                        //Estraggo
                        var _directoryInput = new DirectoryInfo(Configuration.Crif_Configuration.CRIF_Input_WORK);

                        var _ListDirectoryWorkNDG = _directoryInput.GetDirectories($"*{_taxCode}*").ToList();

                        CRIF_GradoRischio _CRIF_GradoRischio = new CRIF_GradoRischio();


                        foreach (var _DirectoryWorkNDG in _ListDirectoryWorkNDG)
                        {
                            //Estraggo il file CSV relativo alla posizione NDG lavorata
                            IEnumerable<FileInfo> _Microdata_NDG_csv = _DirectoryWorkNDG.GetFiles("*.*", SearchOption.AllDirectories).Where(x => x.Name.ToUpper().Contains(_taxCode.ToString()) && x.Extension.ToUpper().Equals(".CSV"));
                            if ((_Microdata_NDG_csv == null) || (_Microdata_NDG_csv.Count() == 0)) { log.Error($"File Microdata_NDG {_taxCode.ToString()} non presente nella cartella: {_DirectoryWorkNDG}"); throw new Exception($"File Microdata_NDG {_taxCode.ToString()} non presente nella cartella: {_DirectoryWorkNDG}"); }
                            FileInfo _fileInfoNDG = _Microdata_NDG_csv.FirstOrDefault();
                            Crif_Excel oCrif_ExcelNDG = new Crif_Excel();
                            var source_KYC_NDG = oCrif_ExcelNDG.load_KYC_Scadenziario(_fileInfoNDG.FullName, 0);
                            var folder_name = Path.GetFileNameWithoutExtension(_fileInfoNDG.Name);
                            var path_DocumentaryFolder = Path.Combine(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY, folder_name);

                            if (!Directory.Exists(path_DocumentaryFolder)) { Directory.CreateDirectory(path_DocumentaryFolder); }
                            //Ciclo sugli elementi contenuti nella cartella
                            foreach (System.Data.DataRow _dr_KYC_NDG in source_KYC_NDG.Rows)
                            {
                                var _tipoDocumento = _dr_KYC_NDG["TIPO_DOCUMENTO"].ToString();
                                var _tipodocIndice = _tipoDocumento.Equals("CRIF_REP") ? "CDC_KYC_QE" : String.Empty;
                                var _filename = _dr_KYC_NDG["FILENAME"].ToString();
                                //Effettuo la verifica

                                var txtIndice = workFileTodocumentary(_dr_KYC_SCADENZIARIO, _dr_KYC_NDG, folder_name, _filename, _tipodocIndice);
                                _lstIndice.Add(txtIndice);

                            }
                            ///Verifica riguardante i dati che permettono la creazione Al kyc del CSV
                            _CRIF_GradoRischio.export_GradoRischio(_dr_KYC_SCADENZIARIO, _DirectoryWorkNDG, ref _Row_Indice, ref _Row_Folder);
                        }

                    }
                }

                if (_lstIndice.Count > 0) { System.IO.File.AppendAllLines(Crif_Configuration.CRIF_INDICE_DOCUMENTARY, _lstIndice); }

            }
            catch (Exception)
            {
                throw;
            }
        }




        /// <summary>
        /// Funzione per creare il file di Indice per la lavorazione
        //  COD_NDG COD_NDG Varchar2(13)
        //TIPO DOCUMENTO  TIPO_DOC Varchar2(10)	Valori ammessi:
        //RICO, STRANIERI, PASSAPORTO, PATENTE, ARMI, POSTALE, PUBBAMM, RICO_ALTRO, CRIF_SELFID, CRIF_FAE
        //DATA ACQUISIZIONE DATA_DOCUMENTO  DateTime
        //COGNOME COGNOME Varchar2(30)	
        //NOME NOME    Varchar2(30)
        //CODICE_FISCALE CODICE_FISCALE  Varchar2(30)
        //DATA_SCADENZA DATA_SCADENZA   DateTime Per i documenti di identità
        //PRIVACY PRIVACY Varchar2(10)    Concatenazione di 5 caratteri come segue:
        //                1) sempre 1
        //Flag 1 Marketing → Flag 2 Modulo Privacy
        //2) marketingFlags - marketingFlag1: 1 se flaggato, altrimenti 0
        //3) marketingFlags - marketingFlag2: 1 se flaggato, altrimenti 0
        //4) marketingFlags - marketingFlag3: 1 se flaggato, altrimenti 0
        //5) sempre 1
        //Esempio: 10111
        //DATA_RICEZIONE DATA_RICEZIONE  DateTime
        //DATA LAVORAZIONE DATA_SCANSIONE  DateTime
        //SESSIONE    NUM_CD Varchar2(30)	
        //SCATOLA NUM_SCATOLA Varchar2(7)
        //PACCO NUM_PACCO   Varchar2(3)
        //PAGINE NUM_PAG INTEGER Non obbligatorio
        //DOCUMENT_TYPE   TYPE_ID Varchar2(10)	Valori ammessi:
        //SPECIMEN, CRIF
        //FILE_TYPE   APP_ID Valori ammessi:
        //                ACROBAT, VIDEO
        //FILENAME    FILENAME Percorso relativo del file
        /// </summary>
        private System.String workFileTodocumentary(System.Data.DataRow _dr_KYC_SCADENZIARIO,
                                                    System.Data.DataRow _dr_KYC_NDG,
                                                    System.String _folderName,
                                                    String _filenameOutput,
                                                    System.String _TipoDocRepo)
        {
            System.Text.StringBuilder _textIndice = new StringBuilder();
            List<String> _ListDocTypes_SPECIMEN = new List<string> { { "RICO" }, { "CRIF_REP" }, { "STRANIERI" }, { "PASSAPORTO" }, { "PATENTE" }, { "ARMI" }, { "POSTALE" }, { "PUBBAMM" }, { "CF" }, { "RICO_ALTRO" } };
            List<String> _ListDocTypes_CRIF = new List<string> { { "CRIF_VIDEO" }, { "CRIF_SELFID" }, { "CRIF_FOTO" }, { "CRIF_FAE" } };
            //inserisco il campo di provenienza
            System.String _Provenienza = string.Empty;
            try
            {
                //Estraggo il campo provenienza dallo scadenziario
                _Provenienza = _dr_KYC_SCADENZIARIO["Provenienza"].ToString();
                decimal _ndg = 0;
                if (_dr_KYC_SCADENZIARIO["NDG"] != System.DBNull.Value)
                {
                    _ndg = Decimal.Parse(_dr_KYC_SCADENZIARIO["NDG"].ToString(), System.Globalization.NumberStyles.Any);
                }
                //FILENAME
                var _filename = _dr_KYC_NDG["FILENAME"].ToString();
                log.Info($"Nome del file da Caricare {_filename}");
                //Composto solo da cartella e file \CARTELLA\FILE
                var _Path_Input_Filename_Documentale = string.Concat(_folderName, "\\", _filename);
                var _Path_Output_Filename_Documentale = string.Concat(_folderName, "\\", _filenameOutput);
                log.Info($"Nome + Cartella da Caricare a documentale {_filename}");
                //contiene tutto il path dove salvare il file da portare a documentale
                var _Path_Output_FileSystem = Path.Combine(Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY, _Path_Output_Filename_Documentale);

                //modifica effettuata perchè a volte ci sono cartelle interne ad altre cartelle.
                System.String _DirectoryDaCopiare = Path.Combine(Crif_Configuration.CRIF_Input_WORK, _folderName);
                if (!new DirectoryInfo(_DirectoryDaCopiare).Exists) { throw new Exception($"Cartella {_folderName} non trovata."); }
                DirectoryInfo _DirectoryInfoDaCopiare = new DirectoryInfo(_DirectoryDaCopiare);

                var _Path_Input_FileSystem = _DirectoryInfoDaCopiare.GetFiles(_filename, SearchOption.AllDirectories).FirstOrDefault();
                if (_Path_Input_FileSystem == null) { throw new Exception($"Il seguente file: {_filename} non trovato"); }

                //var _Path_Input_FileSystem = Path.Combine(Crif_Configuration.CRIF_Input_WORK, _Path_Input_Filename_Documentale);
                log.Info($"Path File System dove salvare il file da Caricare a documentale {_filename}");

                //_textIndice.Append(String.Concat("COD_NDG", ";"));
                if (_ndg != 0) { _textIndice.Append(String.Concat(_ndg.ToString(), ";")); } else { _textIndice.Append(";"); }


                //_textIndice.Append(String.Concat("TIPO DOCUMENTO", ";"));
                var _tipoDocumento = String.IsNullOrEmpty(_TipoDocRepo) ? _dr_KYC_NDG["TIPO_DOCUMENTO"].ToString() : _TipoDocRepo;
                _textIndice.Append(String.Concat(_tipoDocumento.ToString(), ";"));

                //_textIndice.Append(String.Concat("DATA ACQUISIZIONE", ";"));
                //FORMATO DELLA DATA - GGMMAAAA                
                if (_dr_KYC_NDG["DATA_ACQUISIZIONE"] == System.DBNull.Value) { _textIndice.Append(";"); }
                else
                {
                    var _DataAcquisizione = _dr_KYC_NDG["DATA_ACQUISIZIONE"].ToString();
                    var dataFormattata = DateTime.ParseExact(_DataAcquisizione, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    _textIndice.Append(string.Concat(dataFormattata.ToString("yyyyMMdd"), ";"));
                };

                //DateTime.ParseExact("03032020", "ddMMyyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd")

                //_textIndice.Append(String.Concat("COGNOME", ";"));
                var _cognome = _dr_KYC_NDG["COGNOME"].ToString();
                _textIndice.Append(String.Concat(_cognome.ToString(), ";"));

                //_textIndice.Append(String.Concat("NOME", ";"));
                var _nome = _dr_KYC_NDG["NOME"].ToString();
                _textIndice.Append(String.Concat(_nome.ToString(), ";"));

                //_textIndice.Append(String.Concat("CODICE_FISCALE", ";"));
                var _codice_fiscale = _dr_KYC_NDG["CODICE_FISCALE"].ToString();
                _textIndice.Append(String.Concat(_codice_fiscale.ToString(), ";"));

                //_textIndice.Append(String.Concat("DATA_SCADENZA", ";"));expiryDate                                
                if (_dr_KYC_SCADENZIARIO["expiryDate"] == System.DBNull.Value)
                { _textIndice.Append(";"); }
                else
                {
                    var _DataExpiry = _dr_KYC_SCADENZIARIO["expiryDate"].ToString();
                    var dataFormattata = DateTime.ParseExact(_DataExpiry, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    _textIndice.Append(string.Concat(dataFormattata.ToString("yyyyMMdd"), ";"));
                };

                //_textIndice.Append(String.Concat("PRIVACY", ";"));
                var marketingFlag1 = _dr_KYC_SCADENZIARIO["marketingFlag1"].ToString();
                var marketingFlag2 = _dr_KYC_SCADENZIARIO["marketingFlag2"].ToString();
                var marketingFlag3 = _dr_KYC_SCADENZIARIO["marketingFlag3"].ToString();
                System.String _privacy = $"1{marketingFlag1}{marketingFlag2}{marketingFlag3}1";
                _textIndice.Append(String.Concat(_privacy, ";"));

                //_textIndice.Append(String.Concat("DATA_RICEZIONE", ";"));
                _textIndice.Append(String.Concat(System.DateTime.Now.ToString("yyyyMMdd"), ";"));

                //_textIndice.Append(String.Concat("DATA LAVORAZIONE", ";"));
                _textIndice.Append(String.Concat(System.DateTime.Now.ToString("yyyyMMdd"), ";"));

                //_textIndice.Append(String.Concat("SESSIONE", ";"));
                var _sessione = string.Empty;
                if (!string.IsNullOrEmpty(_folderName))
                {
                    var splitSessione = _folderName.Split('_');
                    if (splitSessione.Length > 0) { _sessione = splitSessione[2].ToString(); }
                }
                _textIndice.Append(String.Concat(_sessione, ";"));

                // _textIndice.Append(String.Concat("SCATOLA", ";"));
                _textIndice.Append(String.Concat("", ";"));

                //_textIndice.Append(String.Concat("PACCO", ";"));
                _textIndice.Append(String.Concat("", ";"));

                //_textIndice.Append(String.Concat("PAGINE", ";"));
                _textIndice.Append(String.Concat("", ";"));

                //_textIndice.Append(String.Concat("DOCUMENT_TYPE", ";"));
                var _document_Type = _dr_KYC_NDG["TIPO_DOCUMENTO"].ToString();
                String CodTipoDocumento = String.Empty;
                if (_ListDocTypes_SPECIMEN.Contains(_document_Type))
                {
                    if (_Provenienza.ToUpper().Equals("S"))
                    {
                        //sottodivisione per quando riguarda SPORTELLI/CARTE DI CREDITO
                        CodTipoDocumento = "SPECIMEN";
                    }
                    else if (_Provenienza.ToUpper().Equals("C"))
                    {
                        CodTipoDocumento = "CDC";
                    }
                }
                else if (_ListDocTypes_CRIF.Contains(_document_Type)) { CodTipoDocumento = "CRIF"; }
                _textIndice.Append(String.Concat(CodTipoDocumento.ToString(), ";"));

                //_textIndice.Append(String.Concat("FILE_TYPE", ";"));
                var _file_type = _dr_KYC_NDG["TIPO_DOCUMENTO"].ToString();
                if (_file_type.Equals("CRIF_VIDEO") || _file_type.Equals("CRIF_SELFID")) { _textIndice.Append(String.Concat("VIDEO", ";")); } else { _textIndice.Append(String.Concat("ACROBAT", ";")); }


                //Aggiunta del canale
                if (_Provenienza.ToUpper().Equals("S"))
                {
                    _textIndice.Append("S;");
                }
                else if (_Provenienza.ToUpper().Equals("C"))
                {
                    _textIndice.Append("C;");
                };
                //_textIndice.Append(String.Concat("FILENAME", ";"));
                _textIndice.Append(String.Concat(_Path_Output_Filename_Documentale.ToString(), ";"));

                //Funzione che permette di effettuare il salvataggio nella cartella predefinita del file.
                File.Copy(_Path_Input_FileSystem.FullName, _Path_Output_FileSystem, true);
            }
            catch (Exception)
            {
                throw;
            }
            return _textIndice.ToString();
        }

        #region "invioFileToVarsavia"        
        /// <summary>
        /// Funzione per l'invio del documento contenente il report complessivo a Varsavia
        /// </summary>
        public void SendFileToVarsavia(System.String _pathReportFile)
        {
            try
            {
                log.Info($"Inizio ===> Abilitazione Invio File A Varsavia");
                if (Crif_Configuration.CRIF_ENABLE_VARSAVIA)
                {
                    log.Info($"Send Varsavia File {Path.GetFileName(_pathReportFile)}");
                    //Effettua la copia del file contenente il report lavorato in Varsavia.
                    File.Copy(_pathReportFile, Path.Combine(Crif_Configuration.CRIF_VARSAVIA, Path.GetFileName(_pathReportFile)), true);
                    if (File.Exists(Path.Combine(Crif_Configuration.CRIF_VARSAVIA, Path.GetFileName(_pathReportFile))))
                    {
                        log.Info($"File copiato correttamente ===> {Path.GetFileName(_pathReportFile)}");
                    }
                    log.Info($"Fine ===> Abilitazione Invio File A Varsavia");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region "Delete_File-Folder_Work"
        /// <summary>
        /// Cancella il file che ho utilizzato
        /// </summary>
        public void Delete_File_ToWork(System.String _pathFileScadenziario)
        {
            try
            {
                FileInfo _fieInfo = new FileInfo(_pathFileScadenziario);
                log.Info($"Verifico la presenza del file {_pathFileScadenziario}");
                if (File.Exists(_pathFileScadenziario))
                {
                    log.Info($"Cancello il file {_pathFileScadenziario}");
                    File.Delete(_pathFileScadenziario);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Cancella la cartella che ho utilizzato
        /// </summary>
        /// <param name="_pathFileWork"></param>
        public void Delete_Folder_ToWork(System.String _pathFileWork)
        {
            try
            {
                if (Directory.Exists(_pathFileWork))
                {
                    DirectoryInfo _DirectoryInfo = new DirectoryInfo(_pathFileWork);
                    var _fileInfo = _DirectoryInfo.GetFiles();
                    if (_fileInfo.Length > 0) { foreach (FileInfo _FInfo in _fileInfo) { _FInfo.Delete(); } }
                    _DirectoryInfo.Delete();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public void Delete_Folder_Empty(System.String _pathFolder)
        {
            try
            {
                if (Directory.Exists(_pathFolder))
                {
                    DirectoryInfo _DirectoryInfo = new DirectoryInfo(_pathFolder);
                    var _directoryList = _DirectoryInfo.GetDirectories();
                    if (_directoryList.Length > 0)
                    {
                        foreach (DirectoryInfo _FInfo in _directoryList)
                        {
                            if (_FInfo.GetFiles().Length == 0)
                            {
                                _FInfo.Delete();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



    }
}

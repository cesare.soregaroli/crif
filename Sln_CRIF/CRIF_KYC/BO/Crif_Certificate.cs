﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PGP_Service.Expiry;
using PGP_Service.PrivateKey;
using CRIF_KYC.Mail;
using CRIF_KYC.Configuration;
using System.IO;
using CRIF_KYC.Warning;

namespace CRIF_KYC.Certificate
{

    class Crif_Certificate : Crif_Warning
    {
        /// <summary>
        /// Permette di Tenere traccia degli Warning
        /// </summary>
        public List<Crif_Warning> _Crif_Warning { get; set; }

        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Booleano per invio della mail
        /// </summary>
        private Boolean _email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString());

        /// <summary>
        /// Costruttore
        /// </summary>
        public Crif_Certificate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Crif_Certificate_Verify - Verifica dei Dati per il certificato in questione
        /// </summary>
        /// <returns></returns>
        public Boolean Crif_Certificate_Verify()
        {
            Boolean _ret = true;
            System.Text.StringBuilder _StrBody = new StringBuilder();
            try
            {
                var _pgpVerify = new PGP_Expiry(Crif_Configuration.PGP_Public);
                PGP_Service.Expiry.PGP_Expiry.PgpPublicKeyItem _PgpPublicKeyItem = _pgpVerify.Verify_Expiry();

                if (_PgpPublicKeyItem._DateExpiry.CompareTo(System.DateTime.Now) < 1)
                {
                    log.Error($"Certificato Non Valido - Data di Scadenza {_PgpPublicKeyItem._DateExpiry.ToShortDateString()}");
                    if (_email_sendemail)
                    {
                        _StrBody.Append($"La procedura è stata bloccata causa certificato Scaduto - Scadenza Certificato {_PgpPublicKeyItem._DateExpiry.ToShortDateString()}");
                        _StrBody.Append($"Informare i Sistemisti per rigenerare il certificato ed inviarlo a Deuutsche Bank");
                        Crif_Certificate_Alert_Microframe(_StrBody);
                    }
                    _ret = false;
                }
                else if ((_PgpPublicKeyItem._DateExpiry.CompareTo(System.DateTime.Now) > 0) && (_PgpPublicKeyItem._DateExpiry.Subtract(System.DateTime.Now).TotalDays <= 30))
                {
                    //Invio Alert per Scadenza ravvicinata del certificato
                    log.Warn($"Certificato In Scadenza - Data di Scadenza {_PgpPublicKeyItem._DateExpiry.ToShortDateString()}");
                    if (_email_sendemail)
                    {
                        _StrBody.Append($"La procedura richiede l'aggiornamento del certificato entro la data: {_PgpPublicKeyItem._DateExpiry.ToShortDateString()}. Dopo tale data la procedura sarà bloccata.");
                        _StrBody.Append($"Informare i Sistemisti per rigenerare il certificato ed inviarlo a Deutsche Bank");
                        Crif_Certificate_Alert_Microframe(_StrBody);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _ret;
        }

        /// <summary>
        /// Funzione che permette Invio di una segnalazione a Microframe 
        /// </summary>
        /// <param name="_Block">
        /// - True : Invia segnalazione di blocco della elaborazione a Microframe per certificato scaduto.
        /// - False : Invia segnalazione di Alert a Microframe per la scadenza del certificato.
        /// </param>
        private void Crif_Certificate_Alert_Microframe(System.Text.StringBuilder _StrBody)
        {
            try
            {
                Crif_Mail _Crif_Mail = new Crif_Mail();
                _Crif_Mail.email_from = System.Configuration.ConfigurationManager.AppSettings["email_from"].ToString();
                _Crif_Mail.email_to = System.Configuration.ConfigurationManager.AppSettings["email_to"].ToString();
                _Crif_Mail.email_cc = System.Configuration.ConfigurationManager.AppSettings["email_cc"].ToString();
                _Crif_Mail.email_sendemail = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["email_sendemail"].ToString());
                _Crif_Mail.email_smtpserver = System.Configuration.ConfigurationManager.AppSettings["email_smtpserver"].ToString();
                _Crif_Mail.email_eccezione_to = System.Configuration.ConfigurationManager.AppSettings["email_eccezione_to"].ToString();
                _Crif_Mail.SendMail(true, _StrBody.ToString());
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Funzione per effettuare il Decrypt del file
        /// </summary>
        /// <param name="_fileCrypted">File Cryptato</param>
        public void DecryptFile(FileInfo _fileCrypted) {            
            try
            {
                System.String _pgpPathOutput = Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, Path.GetFileNameWithoutExtension(_fileCrypted.Name));

                if (!Directory.Exists(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED)) { Directory.CreateDirectory(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED);}
                //Fatta questa modifica per permettere di utilizzare l'applicativo anche se non vi sono file CRIPTATI PGP
                if (Crif_Configuration.CRIF_CERTIFICATO_PGP_Enable) {
                    _pgpPathOutput = Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, Path.GetFileNameWithoutExtension(_fileCrypted.Name));
                    var _pgpDecrypt = new PGP_Decrypt(Crif_Configuration.PGP_Private, Crif_Configuration.CRIF_PWD);
                    _pgpDecrypt.decrypt(_fileCrypted, _pgpPathOutput);
                }
                else
                {
                    _pgpPathOutput = Path.Combine(Crif_Configuration.CRIF_Input_SFTP_DECRYPTED, _fileCrypted.Name);
                    File.Copy(_fileCrypted.FullName, _pgpPathOutput, true);
                }
                
                if (File.Exists(_pgpPathOutput))
                {
                    log.Info($"Decript effettuato correttamente nel path : {_pgpPathOutput}");
                }
                else {
                    log.Error($"Decript NON effettuato correttamente nel path : {_pgpPathOutput}");
                    _Crif_Warning.Add(new Crif_Warning() { Warning_Tipo_Progetto = Crif_Configuration.CRIF_Tipo_Esecuzione,
                        Warning_Element = $"Operazione di Decrypt sul file {_fileCrypted.Name} non effettuata. Sussistono problemi su questo archivio" });
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}

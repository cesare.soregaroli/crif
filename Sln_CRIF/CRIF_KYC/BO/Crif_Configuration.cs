﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRIF_KYC.XMLConfigurazione;

namespace CRIF_KYC.Configuration
{

    static class Crif_Configuration
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string _data = DateTime.Now.ToString("yyyyMMdd_HHmmss");


        /// <summary>
        /// Tiene memorizzato il tipo di esecuzione che viene effettuata
        /// </summary>
        static public System.String CRIF_Tipo_Esecuzione { get; set; }

        /// <summary>
        /// Viene abilitato/disabilitato l'esecuzione della procedura
        /// </summary>
        static public System.Boolean CRIF_Tipo_Esecuzione_Enable { get; set; }

        /// <summary>
        /// Parametro utilizzato per Abilitare/Disabilitare la gestione del certificato
        /// </summary>
        static public System.Boolean CRIF_CERTIFICATO_PGP_Enable { get; set; }

        /// <summary>
        /// Path Input SFTP
        /// </summary>
        static public string CRIF_Input_SFTP { get; set; }
        //{
        //    get => System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_SFTP"].ToString();
        //}
        /// <summary>
        /// Path Backup Input SFTP
        /// </summary>
        static public string CRIF_Input_SFTP_BACKUP { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_SFTP_BACKUP"].ToString(),_data);            
        //}
        /// <summary>
        /// 
        /// </summary>
        static public string CRIF_Input_SFTP_DECRYPTED { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_SFTP_DECRYPTED"].ToString(),_data);
        //}

        static public string CRIF_Input_SFTP_DECRYPTED_BACKUP { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_SFTP_DECRYPTED_BACKUP"].ToString(), _data);
        //}

        /// <summary>
        /// Cartella contenente gli scarti inerenti alle posizioni lavorate
        /// </summary>
        static public string CRIF_SCARTI { get; set; }

        /// <summary>
        /// Path Input cartella di Lavoro
        /// </summary>
        static public string CRIF_Input_WORK { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_WORK"].ToString(),_data);
        //}
        /// <summary>
        /// Path Input WORK di BACKUP
        /// </summary>
        static public string CRIF_Input_WORK_BACKUP { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_Input_WORK_BACKUP"].ToString(),_data);
        //}
        /// <summary>
        /// Path OUTPUT Documentary
        /// </summary>
        static public string CRIF_OUTPUT_DOCUMENTARY { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_OUTPUT_DOCUMENTARY"].ToString());
        //}
        /// <summary>
        /// Path contenente il LOG di CRIF
        /// </summary>
        static public string CRIF_LOG_ERROR { get; set; }
        //{
        //    get => Path.Combine(System.Configuration.ConfigurationManager.AppSettings["CRIF_LOG_ERROR"].ToString());
        //}
        
        /// <summary>
        /// Nome del file di indice
        /// </summary>
        static public string CRIF_INDICE_DOCUMENTARY
        { get;set; }

        /// <summary>
        /// abilitazione report da mandfare alla produzione
        /// </summary>
        static public System.Boolean CRIF_ENABLE_REPORT_PROD { get; set; }
        
        /// <summary>
        /// Path dove depositare il report di produzione      
        /// </summary>
        static public string CRIF_REPORT_PROD { get; set; }
        
        /// <summary>
        /// Path dove depositare il backup del report di produzione
        /// </summary>
        static public string CRIF_REPORT_PROD_BACKUP { get; set; }

        /// <summary>
        /// Contiene l'abilitazione dell'invio per varsavia
        /// </summary>
        static public System.Boolean CRIF_ENABLE_VARSAVIA { get; set; }
        
        /// <summary>
        /// Contiene il path di Varsavia
        /// </summary>
        static public string CRIF_VARSAVIA { get; set; }

        /// <summary>
        /// Valore booleano per sapere se il report da inviare a KYC è corretto
        /// </summary>
        public static Boolean CRIF_ENABLE_REPORT_KYC { get; set; }

        /// <summary>
        /// Path di deposito per KYC
        /// </summary>
        static public string CRIF_REPORT_KYC { get; set; }

        
        /// <summary>
        /// Stringa di connessione Oracle
        /// </summary>
        static public string CONNECTION_ORACLE { get; set; }
        /// <summary>
        /// Stringa di connessione SQL server
        /// </summary>
        static public string CONNECTION_SQLSERVER { get; set; }

        /// <summary>
        /// Path certificato Pubblico
        /// </summary>
        static public string PGP_Public
        {
            get => Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["PGP_Public"].ToString());
        }

        /// <summary>
        /// Path certificato Privato
        /// </summary>
        static public string PGP_Private
        {            
            get => Path.Combine(System.Environment.CurrentDirectory, System.Configuration.ConfigurationManager.AppSettings["PGP_Private"].ToString());
        }

        /// <summary>
        /// CRIF Password
        /// </summary>
        static public string CRIF_PWD
        {
            get => System.Configuration.ConfigurationManager.AppSettings["CRIF_PWD"].ToString();
        }

        /// <summary>
        /// Stampo Il contenuto dei parametri settati
        /// </summary>
        static public void LogParameterAttribute()
        {
            try
            {
                //log.Info($"Path SFTP : {Crif_Configuration.CRIF_Input_SFTP.ToString()}");
                //log.Info($"Path SFTP Backup : {Crif_Configuration.CRIF_Input_SFTP_BACKUP.ToString()}");
                //log.Info($"Path SFTP Decrypted : {Crif_Configuration.CRIF_Input_SFTP_DECRYPTED.ToString()}");                
                //log.Info($"Path Work : {Crif_Configuration.CRIF_Input_WORK.ToString()}");
                //log.Info($"Path Work Backup : {Crif_Configuration.CRIF_Input_WORK_BACKUP.ToString()}");
                //log.Info($"Path Documentary : {Crif_Configuration.CRIF_OUTPUT_DOCUMENTARY.ToString()}");
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Effettuo la verifica se le tabelle Principali sono state Create
        /// </summary>
        static public void VerifyPrincipalFolder(CRIF_Project _CRIF_Project) {
            try
            {

                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_Input_SFTP_BACKUP) && !Directory.Exists(_CRIF_Project.CRIF_Input_SFTP_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.CRIF_Input_SFTP_BACKUP); }                
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED) && !Directory.Exists(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED)) { Directory.CreateDirectory(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_Input_WORK) && !Directory.Exists(_CRIF_Project.CRIF_Input_WORK)) { Directory.CreateDirectory(_CRIF_Project.CRIF_Input_WORK); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_Input_WORK_BACKUP) && !Directory.Exists(_CRIF_Project.CRIF_Input_WORK_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.CRIF_Input_WORK_BACKUP); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_OUTPUT_DOCUMENTARY) && !Directory.Exists(_CRIF_Project.CRIF_OUTPUT_DOCUMENTARY)) { Directory.CreateDirectory(_CRIF_Project.CRIF_OUTPUT_DOCUMENTARY); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED_BACKUP) && !Directory.Exists(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED_BACKUP); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_SCARTI) && !Directory.Exists(_CRIF_Project.CRIF_SCARTI)) { Directory.CreateDirectory(_CRIF_Project.CRIF_SCARTI); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_LOG_ERROR) && !Directory.Exists(_CRIF_Project.CRIF_LOG_ERROR)) { Directory.CreateDirectory(_CRIF_Project.CRIF_LOG_ERROR); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_REPORT_PROD) && !Directory.Exists(_CRIF_Project.CRIF_REPORT_PROD)) { Directory.CreateDirectory(_CRIF_Project.CRIF_REPORT_PROD); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_REPORT_PROD_BACKUP) && !Directory.Exists(_CRIF_Project.CRIF_REPORT_PROD_BACKUP)) { Directory.CreateDirectory(_CRIF_Project.CRIF_REPORT_PROD_BACKUP); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_VARSAVIA) && !Directory.Exists(_CRIF_Project.CRIF_VARSAVIA)) { Directory.CreateDirectory(_CRIF_Project.CRIF_VARSAVIA); }
                if (!string.IsNullOrEmpty(_CRIF_Project.CRIF_REPORT_KYC) && !Directory.Exists(_CRIF_Project.CRIF_REPORT_KYC)) { Directory.CreateDirectory(_CRIF_Project.CRIF_REPORT_KYC); }

                

                CRIF_Tipo_Esecuzione = _CRIF_Project.CRIF_Tipo_Esecuzione;
                CRIF_Tipo_Esecuzione_Enable =  _CRIF_Project.CRIF_Tipo_Esecuzione_Enable;
                CRIF_Input_SFTP = _CRIF_Project.CRIF_Input_SFTP;
                CRIF_Input_SFTP_BACKUP = Path.Combine(_CRIF_Project.CRIF_Input_SFTP_BACKUP, _data);
                CRIF_Input_SFTP_DECRYPTED = Path.Combine(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED, _data);
                CRIF_Input_SFTP_DECRYPTED_BACKUP = Path.Combine(_CRIF_Project.CRIF_Input_SFTP_DECRYPTED_BACKUP, _data);
                CRIF_SCARTI = Path.Combine(_CRIF_Project.CRIF_SCARTI, _data);
                CRIF_Input_WORK = Path.Combine(_CRIF_Project.CRIF_Input_WORK, _data);
                CRIF_Input_WORK_BACKUP = Path.Combine(_CRIF_Project.CRIF_Input_WORK_BACKUP, _data);
                CRIF_ENABLE_REPORT_PROD = _CRIF_Project.CRIF_ENABLE_REPORT_PROD;
                CRIF_REPORT_PROD = _CRIF_Project.CRIF_REPORT_PROD;
                CRIF_REPORT_PROD_BACKUP = Path.Combine(_CRIF_Project.CRIF_REPORT_PROD_BACKUP, _data);
                CRIF_OUTPUT_DOCUMENTARY = Path.Combine(_CRIF_Project.CRIF_OUTPUT_DOCUMENTARY, _data);
                CRIF_LOG_ERROR = Path.Combine(_CRIF_Project.CRIF_LOG_ERROR, _data);
                CRIF_INDICE_DOCUMENTARY = _CRIF_Project.CRIF_LOG_ERROR;
                CRIF_ENABLE_VARSAVIA = _CRIF_Project.CRIF_ENABLE_VARSAVIA;
                CRIF_VARSAVIA = _CRIF_Project.CRIF_VARSAVIA;
                CONNECTION_ORACLE = _CRIF_Project.CONNECTION_ORACLE;
                CONNECTION_SQLSERVER = _CRIF_Project.CONNECTION_SQLSERVER;
                CRIF_CERTIFICATO_PGP_Enable = _CRIF_Project.CRIF_CERTIFICATO_PGP_Enable;
                CRIF_REPORT_KYC = Path.Combine(_CRIF_Project.CRIF_REPORT_KYC, _data);
                CRIF_ENABLE_REPORT_KYC = _CRIF_Project.CRIF_ENABLE_REPORT_KYC;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

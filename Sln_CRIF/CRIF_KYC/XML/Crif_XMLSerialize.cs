﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using CRIF_KYC.XMLConfigurazione;

namespace CRIF_KYC.XMLSerialize
{
    
    class Crif_XMLSerialize
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public CRIF SerializeXMLToCRIF_Project()
        {
            CRIF _oCRIF = null;
            string path = string.Empty;
            try
            {
                path =  System.Configuration.ConfigurationManager.AppSettings["XML_Config"].ToString();
                if (new FileInfo(path).Exists)
                {
                    XmlSerializer ser = new XmlSerializer(typeof(CRIF));

                    using (Stream reader = new FileStream(path, FileMode.Open))
                    {
                        // Call the Deserialize method to restore the object's state.
                        _oCRIF = (CRIF)ser.Deserialize(reader);
                    }
                }
                else
                {
                    throw new Exception($"File di configurazione non trovato { path } ");
                }
            }
            catch (Exception)
            {                
                throw;
            }
            return _oCRIF;
        }
    }
}

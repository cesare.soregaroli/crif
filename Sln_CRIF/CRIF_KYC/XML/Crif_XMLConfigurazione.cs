﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CRIF_KYC.XMLConfigurazione
{
    [XmlRoot(ElementName = "CRIF_MAIL")]
    public class CRIF_MAIL
    {
        [XmlElement(ElementName = "CRIF_Project_sendemail")]
        public System.Boolean CRIF_Project_sendemail { get; set; }
        [XmlElement(ElementName = "CRIF_Project_smtpserver")]
        public string CRIF_Project_smtpserver { get; set; }
        [XmlElement(ElementName = "CRIF_Project_from")]
        public string CRIF_Project_from { get; set; }
        [XmlElement(ElementName = "CRIF_Project_to")]
        public string CRIF_Project_to { get; set; }
        [XmlElement(ElementName = "CRIF_Project_cc")]
        public string CRIF_Project_cc { get; set; }
    }
    [XmlRoot(ElementName = "CRIF_Project")]
    public class CRIF_Project
    {
        [XmlElement(ElementName = "CRIF_Tipo_Esecuzione")]
        public string CRIF_Tipo_Esecuzione { get; set; }

        [XmlElement(ElementName = "CRIF_Tipo_Esecuzione_Enable")]
        public Boolean CRIF_Tipo_Esecuzione_Enable { get; set; }

        [XmlElement(ElementName = "CRIF_CERTIFICATO_PGP_Enable")]
        public Boolean CRIF_CERTIFICATO_PGP_Enable { get; set; }

        [XmlElement(ElementName = "CRIF_Input_SFTP")]
        public string CRIF_Input_SFTP { get; set; }

        [XmlElement(ElementName = "CRIF_Input_SFTP_BACKUP")]
        public string CRIF_Input_SFTP_BACKUP { get; set; }

        [XmlElement(ElementName = "CRIF_Input_SFTP_DECRYPTED")]
        public string CRIF_Input_SFTP_DECRYPTED { get; set; }

        [XmlElement(ElementName = "CRIF_Input_SFTP_DECRYPTED_BACKUP")]
        public string CRIF_Input_SFTP_DECRYPTED_BACKUP { get; set; }

        [XmlElement(ElementName = "CRIF_SCARTI")]
        public string CRIF_SCARTI { get; set; }
        
        
        [XmlElement(ElementName = "CRIF_Input_WORK")]
        public string CRIF_Input_WORK { get; set; }

        [XmlElement(ElementName = "CRIF_Input_WORK_BACKUP")]
        public string CRIF_Input_WORK_BACKUP { get; set; }

        [XmlElement(ElementName = "CRIF_OUTPUT_DOCUMENTARY")]
        public string CRIF_OUTPUT_DOCUMENTARY { get; set; }

        [XmlElement(ElementName = "CRIF_LOG_ERROR")]
        public string CRIF_LOG_ERROR { get; set; }

        [XmlElement(ElementName = "CRIF_INDICE_DOCUMENTARY")]
        public string CRIF_INDICE_DOCUMENTARY { get; set; }

        [XmlElement(ElementName = "CRIF_ENABLE_REPORT_PROD")]
        public Boolean CRIF_ENABLE_REPORT_PROD { get; set; }

        [XmlElement(ElementName = "CRIF_REPORT_PROD")]
        public string CRIF_REPORT_PROD { get; set; }

        [XmlElement(ElementName = "CRIF_REPORT_PROD_BACKUP")]
        public string CRIF_REPORT_PROD_BACKUP { get; set; }

        [XmlElement(ElementName = "CRIF_ENABLE_VARSAVIA")]
        public Boolean CRIF_ENABLE_VARSAVIA { get; set; }

        [XmlElement(ElementName = "CRIF_REPORT_KYC")]
        public System.String CRIF_REPORT_KYC { get; set; }

        [XmlElement(ElementName = "CRIF_ENABLE_REPORT_KYC")]
        public System.Boolean CRIF_ENABLE_REPORT_KYC { get; set; }

        [XmlElement(ElementName = "CONNECTION_ORACLE")]
        public string CONNECTION_ORACLE { get; set; }

        [XmlElement(ElementName = "CONNECTION_SQLSERVER")]
        public string CONNECTION_SQLSERVER { get; set; }

        [XmlElement(ElementName = "CRIF_VARSAVIA")]
        public string CRIF_VARSAVIA { get; set; }


        [XmlElement(ElementName = "CRIF_MAIL")]

        public CRIF_MAIL CRIF_MAIL { get; set; }
    }

    [XmlRoot(ElementName = "CRIF")]
    public class CRIF
    {
        [XmlElement(ElementName = "CRIF_Project")]
        public List<CRIF_Project> CRIF_Project { get; set; }
    }

}
